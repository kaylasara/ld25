package com.refrag.snow
{
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class SnowAtlas extends TextureAtlas
	{
		public function SnowAtlas(texture:Texture, atlasXml:XML=null)
		{
			super(texture, atlasXml);
		}
	}
}