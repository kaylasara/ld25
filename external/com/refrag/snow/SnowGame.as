package com.refrag.snow
{
	import com.refrag.snow.system.Console;
	import com.refrag.snow.system.GameContainer;
	
	import flash.display.BitmapData;
	import flash.display3D.Context3D;
	import flash.geom.Rectangle;
	
	import starling.core.RenderSupport;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.events.EnterFrameEvent;
	import starling.textures.Texture;
	
	public class SnowGame
	{
		public static var width:Number;
		public static var height:Number;
		
		public static var stage:Stage;
		
		//Storing our various game states
		public static var currentState:SnowState;
		public static var previousState:SnowState;
		private static var states:Vector.<SnowState>;
		
		public static var PAUSED:Boolean = false
		public static var TIME:Number;
		
		public static var ELAPSED:Number;
		public static var MUTE:Boolean = false;
		public static var VOLUME:Number = 1;
		
		//FPS stuff
		public static var TARGET_FPS:Number = 60;
		private static var _ACTUAL_FPS:Number = TARGET_FPS;
		private static var _last60Frames:Vector.<Number>;
		
		//Quake stuff
		private static var quake_volatility:Number;
		private static var quake_time:Number;
		
		//Flash Stuff
		private static var flashQuad:Quad;
		private static var flashTime:Number;
		private static var flashTimeMax:Number;
		
		//Blur Stuff
		private static var blurImage:Image;
		
		//Our game container Sprite
		public static var GAME:GameContainer = new GameContainer();

		private static var CONSOLE:Console = new Console();
		
		/**
		 * Constructor should never be called
		 */
		public function SnowGame(stage:Stage)
		{
		}
		
		/**
		 * Calculates the average FPS for the last 60 frames
		 */
		public static function get ACTUAL_FPS():Number
		{
			var sum:Number = 0;
			
			for (var i:int=0;i<_last60Frames.length;i++){
				sum = sum + _last60Frames[i];
			}
			var avg:Number = 1 / (sum/_last60Frames.length);
			_ACTUAL_FPS = avg;
			return _ACTUAL_FPS;
		}
		
		
		/**
		 * Starts the Game
		 * 
		 * @param stage A Starling Stage object
		 */
		public static function start(s:Stage):void
		{
			states = new Vector.<SnowState>;
			stage = s;
			width = stage.stageWidth;
			height = stage.stageHeight;
			_last60Frames = new Vector.<Number>;
			stage.addChild(GAME);
			//stage.addChild(CONSOLE);

			flashQuad = new Quad(width,height,0xFFFFFF);
			flashQuad.touchable = false;
			flashQuad.alpha =0;
			stage.addChild(flashQuad);
		}
		
		/** 
		 *Switches between states
		 * 
		 * @param state The target SnowState to switch to
		 * 
		 */
		public static function switchState(state:SnowState):void
		{
			
			//If I haven't seen this state before, I need to add it to the list and add to the display list
			if (states.indexOf(state) == -1){
				states.push(state);
				GAME.addChild(state);

			}
			previousState = currentState;
			
			if (previousState != null)
				previousState.visible=false;
			

			currentState = state;
			currentState.visible = true;
			trace("In SnowGame...");

			currentState.switchedTo();
		}
		
		/**
		 * Should only be called from the GameContainer
		 */
		public static function update(e:EnterFrameEvent):void
		{
			ELAPSED = e.passedTime;
			_last60Frames.push(e.passedTime);
			if (_last60Frames.length >60)
				_last60Frames.shift();
			
			if (quake_time>0){
				GAME.x= Math.random()*quake_volatility;
				GAME.y= Math.random()*quake_volatility;
				quake_time -= e.passedTime;
			}else {
				GAME.x=0;
				GAME.y=0;
			}
			
			if (flashTime>0){
				trace(flashTime);
				flashQuad.alpha = flashTime/flashTimeMax;
				flashTime -= e.passedTime;
			}else flashQuad.alpha = 0;
			currentState.updateFrame();
		}
		
		public static function quake(volatility:Number=10,time:Number=1):void
		{
			quake_time = time;
			quake_volatility = volatility;
		}
		
		public static function flash(time:Number=0.25, color:uint = 0xFFFFFF):void
		{
			flashTime = time;
			flashTimeMax = time;
			flashQuad.color = color;
		}
		
		public static function blur(displayObject:DisplayObject,offset:Rectangle, time:Number=0.25):void
		{
			//blurTime = time;
			//blurTimeMax = time;
			blurImage = new Image(Texture.fromBitmapData(copyAsBitmapData(displayObject)));
			//blurImage.x = offset.width/2;
			//blurImage.y = offset.height/2;
				
			stage.addChild(blurImage);
		}
		
		public static function copyAsBitmapData(sprite:starling.display.DisplayObject):BitmapData {
			if (sprite == null) return null;
			var resultRect:Rectangle = new Rectangle();
			sprite.getBounds(sprite, resultRect);
			var context:Context3D = Starling.context;
			var support:RenderSupport = new RenderSupport();
			RenderSupport.clear();
			support.setOrthographicProjection(Starling.current.stage.stageWidth, Starling.current.stage.stageHeight);
			support.transformMatrix(sprite.root);
			support.translateMatrix( -resultRect.x, -resultRect.y);
			var result:BitmapData = new BitmapData(resultRect.width, resultRect.height, true, 0x00000000);
			support.pushMatrix();
			support.transformMatrix(sprite);
			sprite.render(support, 1.0);
			support.popMatrix();
			support.finishQuadBatch();
			context.drawToBitmapData(result);
			return result;
		}

	}
}