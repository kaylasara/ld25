package com.refrag.snow
{
	import com.refrag.snow.SnowBody;
	import com.refrag.snow.dialog.ChatManager;
	import com.refrag.snow.dialog.Conversation;
	
	import flash.display.Bitmap;
	import flash.utils.ByteArray;
	
	import nape.geom.Vec2;
	import nape.phys.Body;
	
	import starling.animation.Juggler;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.textures.TextureSmoothing;
	
	import com.refrag.snow.dialog.ChatManager;
	import com.refrag.snow.dialog.Conversation;
	
	import flash.utils.ByteArray;

	public class SnowPhysSprite
	{
		public var conv:Conversation;

		
		//Reference to my Nape Phys Body
		public var sprite:Sprite;
		public var _body:Body;
		private var _x:int;
		private var _y:int;
		
		private var _velocityX:Number;
		private var _velocityY:Number;
		
		public var image:Image;
		
		public var snowBody:SnowBody;
		
		private var _rotate:Number;
		private var _facing:Number;
		private var lastFacing:Number;
		
		
		private var jumpAnim:MovieClip;


		public var allowRotation:Boolean;
		public var maxVelocity:int;
		
		private var _onFloor:Boolean;
		private var _prevY:Number;
		private var _prevX:Number;
		public var touchingFloor:Boolean;
		public var leavingFloor:Boolean;
		private var _scale:Number;
		
		private var _dest:Vec2;
		private var _autoPilot:int;
		
		private var _oldCoordinates:Vec2;
		private var _pilotPatience:int;
		public var maxPilotPatience:int
		public var friction:Number;


		private var _juggler:Juggler;
		
		private var _animationLegend:Vector.<String>;
		private var _animations:Vector.<MovieClip>;
		private var _currentAnim:MovieClip; //Pointer to current Anim
		
		public var dead:Boolean;
		
		private var sTextureAtlas:TextureAtlas;
		
		private var size:Vec2;
		
		public var scorable:Boolean;
		
		public var conversation:Conversation;

		
		public function SnowPhysSprite()
		{
			
			sprite = new Sprite();
			scorable = true;
			dead = false;
			_animations = new Vector.<MovieClip>;
			_animationLegend = new Vector.<String>;
			
			_juggler = new Juggler();

			friction = 0.95;
			maxPilotPatience = 25;
			_pilotPatience = maxPilotPatience;
			_dest = new Vec2(0,0);
			_oldCoordinates = new Vec2(0,0);
			_facing = 1;
			lastFacing = 1;
			allowRotation = true;
			sprite.touchable = false;
			maxVelocity = 1000;

		}
		
		public function setConversation(xml:ByteArray):void
		{
			ChatManager.initialize(xml);
			conv = ChatManager.getCurrentConvo();
			conv.gotoID(0);
		}
		
		public function get velocityX():Number
		{
			_velocityX = snowBody.napeBody.velocity.x;
			return _velocityX;
		}

		public function set velocityX(value:Number):void
		{
			
			_velocityX = value;
			snowBody.napeBody.velocity.x = _velocityX;
 
		}

		public function get velocityY():Number
		{
			_velocityY = snowBody.napeBody.velocity.y;
			return _velocityY;
		}
		
		public function set velocityY(value:Number):void
		{
			
			_velocityY = value;
			snowBody.napeBody.velocity.y = _velocityY;
			
		}
		public function addBody(b:SnowBody):void
		{
			snowBody = b;
		}
		public function setAtlas(a:TextureAtlas, s:Number =1,width:Number=0,height:Number=0, prefix:String = ""):void{
			sTextureAtlas = a;
			size = new Vec2(width,height);
			_scale = s;

		}
		
		public function setImage(i:Bitmap,x:XML=null,s:Number=1,width:Number = 0, height:Number = 0):void{
			
			size = new Vec2(width,height);
			//pivotX = size.x >>1;
			//pivotY = size.y >>1;
			_scale = s;
		
			var bitmap:Bitmap = i;
			var texture:Texture = Texture.fromBitmap(bitmap);
			
			if (x == null){
				image=new Image(Texture.fromBitmap(i));
				sprite.addChild(image);
			}
			else{
				var xml:XML = XML(x);
				
				sTextureAtlas = new TextureAtlas(texture, xml);
			}
			
			
		}
		
		public function setAnim(s:String,repeat:Boolean=true,start:Boolean=false):Boolean{
			var anim:MovieClip;
			try{
				var frames:Vector.<Texture> = sTextureAtlas.getTextures(s);
				
			}catch(e:Error){
				return (false);
			}
			
			anim = new MovieClip(frames, SnowGame.TARGET_FPS/2);			
			anim.pivotX = anim.width >>1;
			anim.pivotY = anim.height >>1;			
			anim.scaleX = _scale;
			anim.scaleY = _scale;
			anim.smoothing = TextureSmoothing.NONE;
			
			_animationLegend.push(s);
			_animations.push(anim);
			
			anim.loop = repeat;
			
			if (size.x==0 && start==true){
				size.x=anim.width;
				size.y=anim.height;
				sprite.pivotX = 0;//size.x;// >>1;
				sprite.pivotY = 0;//size.y;// >>1;
			}
			
			if (start)
				play(s);
				
			
			return(true);
		}
		
		public function play(s:String):void{
			if (_animationLegend.length==0)
				return;
			if (_currentAnim != _animations[_animationLegend.indexOf(s)])
			{
				if (_currentAnim !=null)
				{
					
					sprite.removeChild(_currentAnim);
					_juggler.remove(_currentAnim);
				}
				
				_currentAnim = _animations[_animationLegend.indexOf(s)];
				sprite.addChild(_currentAnim);
				_juggler.add(_currentAnim);
			}
			
			
		}
		
		public function stop(s:String):void{
			_juggler.remove(_currentAnim);
			sprite.removeChild(_currentAnim);
			_currentAnim = _animations[_animationLegend.indexOf(s)];
			sprite.addChild(_currentAnim);

		}
		
		public function moveLeft():void
		{
			if (Math.abs(_body.velocity.x)<maxVelocity)
				_body.velocity.x = _body.velocity.x - 30;
			_facing=-1;
		}

		
		public function moveRight():void
		{
			if (Math.abs(_body.velocity.x)<maxVelocity)
				_body.velocity.x = _body.velocity.x + 30;
			_facing=1;
		}
		
		public function moveTo(destination:Vec2):Boolean
		{
			_dest = destination;
			if (destination.x>_body.position.x)
				_autoPilot = 1;
			else if (destination.x<_body.position.x)
				_autoPilot = -1;
			else{
				_autoPilot=0;
				return(false)
			}
			return(true);//Can move
		}

		public function get body():Body
		{
			return _body;
		}

		public function set body(value:Body):void
		{
			_body = value;
		}

		public function get rotate():Number
		{
			return _rotate;
		}

		public function set rotate(value:Number):void
		{
			if (allowRotation){
				sprite.rotation = value;
			}
		}
		
		public function update():void
		{

			_juggler.advanceTime(1/SnowGame.TARGET_FPS);
			if (touchingFloor) 
				testFloor(true);
			else if(leavingFloor)
				_onFloor=false;
			
			touchingFloor=false;
			leavingFloor=false;
			if (lastFacing!=_facing){
				//runAnim.scaleX=_facing*_scale;
				lastFacing=_facing;
			}
			if (_autoPilot!=0) pilot();
			
			body.velocity.x=body.velocity.x*friction;
			if (dead){
				if (scorable)
					play("hit");
				else
					play("un_hit");
				
				
			}else{
			if (body.velocity.y<0 && !_onFloor){
				if (scorable)
					play("jump");
				else
					play("un_jump");
			}else if (body.velocity.y>0 && !_onFloor){
				if (scorable)
					play("fall");
				else
					play("un_fall");
			}
				
			else{
				if (scorable)
					play("skate");
				else
					play("un_skate");
			}
			}

		}
		
		private function pilot():void{
			if (_dest.x>_body.position.x && _autoPilot==1)
				moveRight();
			else if (_dest.x<_body.position.x && _autoPilot==-1)
				moveLeft();
			else{
				_body.velocity.x=0;
				_autoPilot=0;
			}
			
			
			if (Math.abs(_oldCoordinates.x-body.position.x)<1)
				_pilotPatience--;
			
			if (_pilotPatience==0){
				_autoPilot=0;
				_pilotPatience = maxPilotPatience;
			}
			_oldCoordinates.x = body.position.x;
			
		}

		public function get onFloor():Boolean
		{
			return _onFloor;
		}
		
		public function testFloor(value:Boolean):void
		{
			//trace("Testing Collision...");
			//Check to see if we're trying to stick to a floor
			if (value && !_onFloor){
				//Ok so we know we aren't on a floor yet...
				
				if (!_onFloor && Math.floor(_prevY)==Math.floor(y)){
					//If we are being passed a "true" again, AND our Y is equal between frames..
					//We totally must be on the floor
					//trace("Totally on floor");
					_onFloor = value;
				}
				else{
					//trace("Maybe on Floor?" + (_prevY - y));
					//We'll save this frame's Y to compare.
					_prevY=y;
				}
				//Handle leaving the floor
			} else if (!value && _onFloor){
				_onFloor = value;
			}
		}
		public function set onFloor(value:Boolean):void
		{
			//Check to see if we're trying to stick to a floor
			if (value && !_onFloor){
				//Ok so we know we aren't on a floor yet...
				if (!_onFloor && _prevY==y){
					//If we are being passed a "true" again, AND our Y is equal between frames..
					//We totally must be on the floor
					
					_onFloor = value;
					if (scorable)
						play("skate");
					else
						play("un_skate");
				}
				else{
				//We'll save this frame's Y to compare.
					_prevY=y;
				}
			//Handle leaving the floor
			} else if (!value && _onFloor){
				_onFloor = value;
			}
		}

		public function get facing():Number
		{
			return _facing;
		}

		public function set facing(value:Number):void
		{
			_facing = value;
		}

		public function get y():int
		{
			_y=snowBody.napeBody.position.y;
			return _y;
		}

		public function set y(value:int):void
		{
			_y = value;
			snowBody.napeBody.position.y = _y;
		}

		public function get x():int
		{
			_x=snowBody.napeBody.position.x;

			return _x;
		}

		public function set x(value:int):void
		{
			_x = value;
			snowBody.napeBody.position.x = _x;

		}


	}
}