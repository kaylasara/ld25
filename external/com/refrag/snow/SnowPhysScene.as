package com.refrag.snow
{
	import com.refrag.snow.input.Keyboard;
	
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	
	import nape.callbacks.CbEvent;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.space.Space;
	
	import starling.display.DisplayObject;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.KeyboardEvent;
	
	public class SnowPhysScene extends SnowState
	{
		//Nape Space
		public var space:Space;
		
		//Camera Stuff
		public var viewPort:Rectangle;
		public var offset:Vec2;
		public var cameraBounds:Rectangle;
		
		
		private var _deathrow:Vector.<Body>;
		public static var keys:Keyboard;
		public var simSpeed:Number;
	
		private var _touchPanel:Quad;
		private var _children:Vector.<SnowPhysSprite>;
		private var _graphics:Vector.<DisplayObject>;
		
		public var world:Sprite;

		public var timeDialation:Number = 1;
		
		//Sprites and things
		private var spritePane:Sprite;
		public var platformPane:Sprite;
		
		public function SnowPhysScene()
		{
			_touchPanel = new Quad(Capabilities.screenResolutionX*2, Capabilities.screenResolutionY*2, 0x00004059,false);
			_touchPanel.alpha = 0;
			addChild (_touchPanel);
			_children = new Vector.<SnowPhysSprite>;
			simSpeed=1;
			keys = new Keyboard();
			keys.reset();
			
			//addEventListener(Event.ADDED_TO_STAGE, init);

			

		}
		

		public function init():void
		{
			world = new Sprite();
			spritePane = new Sprite();
			platformPane = new Sprite();
			
			world.addChild(platformPane);
			world.addChild(spritePane);
			
			_children = new Vector.<SnowPhysSprite>;
			_graphics = new Vector.<DisplayObject>;
			simSpeed=1;
			keys = new Keyboard();
			keys.reset();
			
			//Create our camera objects
			viewPort = new Rectangle(0,0,stage.stageWidth,stage.stageHeight);
			offset = new Vec2(stage.stageWidth>>1,stage.stageHeight>>1);
			//cameraBounds = new Rectangle(-100,0,stage.stageWidth+100,stage.stageHeight);
			
			//Create a blank quad for capturing all touch inputs

			addChild (world);
			//addChild (spritePane);
			_deathrow = new Vector.<Body>;
			space = new Space(new Vec2(0, 2000));
			
		//	addEventListener(Event.ENTER_FRAME, loop);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, handleKeyUp);

			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, SnowBody.DYNAMIC, SnowBody.WORLD, handler));
			space.listeners.add(new InteractionListener(CbEvent.ONGOING, InteractionType.COLLISION, SnowBody.DYNAMIC, SnowBody.WORLD, onFloor));
			space.listeners.add(new InteractionListener(CbEvent.END, InteractionType.COLLISION, SnowBody.DYNAMIC, SnowBody.WORLD, offFloor));
		}
		
		private function handler(i:InteractionCallback):void {
			//trace("touching?");
			_children[_graphics.indexOf(i.int1.castBody.graphic)].touchingFloor = true;
		}
		
		private function onFloor(i:InteractionCallback):void {
			_children[_graphics.indexOf(i.int1.castBody.graphic)].touchingFloor = true;
		}
		private function offFloor(i:InteractionCallback):void {
			_children[_graphics.indexOf(i.int1.castBody.graphic)].leavingFloor = true;
		}
		
		private function handleKeyDown(e:KeyboardEvent):void
		{
			keys.handleKeyDown(e);			
		}
		
		private function handleKeyUp(e:KeyboardEvent):void
		{
			keys.handleKeyUp(e);			
		}
		
		private function cleanUp():void
		{
			var l:int = _deathrow.length;
			var b:Body;
			
			for (var i:int=0;i<l;i++){
				b=_deathrow.pop();
				b.graphicUpdate = null;
				spritePane.removeChild(b.graphic,true);
				b.space = null;
				b.clear();
				b.shapes.clear();
				b.shapes.empty();
				space.bodies.remove(b);
				
				
			}
		}
		
		public function update():void
		{
			//simSpeed = DataStore.FPS;
			//space.step(1/((DataStore.FPS)/simSpeed));
			//if (DataStore.passedTime<(1/DataStore.FPS))
			//	space.step(DataStore.passedTime);
			//else
			space.step(1/SnowGame.TARGET_FPS*timeDialation);
			cleanUp();	
			keys.update();
			world.x = Math.floor(-offset.x + (stage.stageWidth>>1));
			world.y = Math.floor(-offset.y + (stage.stageHeight>>1));
			
			for (var i:int;i<_children.length;i++){
				_children[i].update();
				
			}
			
		}
		
		private function updateGraphics(b:Body):void
		{
			b.graphic.x = b.position.x;
			b.graphic.y = b.position.y;
		//	b.graphic.rotate = b.rotation;
		
			
		//	if (b.graphic.isDead())
			//	_deathrow.push(b);
		}
		
		private function updateSimple(b:Body):void
		{
			//b.graphic.x = b.position.x;
			//b.graphic.y = b.position.y;
		}

		
		/*public function newBody(b:Body, g:SnowPhysSprite=null):void
		{
			b.space = space;
			if (g){
				b.graphic = g;
				b.graphicUpdate = updateGraphics;
				
				g.body=b;
			//	spritePane.addChild(g);
				_children.push(g);
			} 
		}*/
		
		public function addEntity(s:SnowPhysSprite):void
		{
			s.snowBody.napeBody.space = space;
			s.snowBody.napeBody.graphic = s.sprite;
			s.snowBody.napeBody.graphicUpdate = updateGraphics;
			
			s.body=s.snowBody.napeBody;
			spritePane.addChild(s.sprite);
			_children.push(s);
			_graphics.push(s.sprite);
			
		}
		
		public function newSimple(b:Body, g:DisplayObject, v:Vec2):void
		{	
			b.position = v;
			b.space = space;
			b.graphic = g;
			b.graphicUpdate = updateSimple;
			b.graphic.x = b.position.x;
			b.graphic.y = b.position.y;
			platformPane.addChild(b.graphic);
			//world.flatten();

		}
		
		override public function add(o:SnowObject):void
		{
		
			platformPane.addChild(o.sprite);
		}
		
		public function refocus(target:Vec2,speed:Number):void
		{
			offset.x=offset.x - (offset.x-target.x)*speed;
			offset.y=offset.y - (offset.y-target.y)*speed;
			viewPort.x = offset.x - (viewPort.width>>1);
			viewPort.y = offset.y - (viewPort.height>>1);
			
			if (cameraBounds){
				if (viewPort.x<cameraBounds.x){
					offset.x = cameraBounds.x + (viewPort.width>>1);
					viewPort.x = cameraBounds.x;
				}else if(viewPort.x+viewPort.width>cameraBounds.width){
					offset.x = cameraBounds.width - (viewPort.width>>1);
					viewPort.x = cameraBounds.width-viewPort.width;
				}
				
				if (viewPort.y<cameraBounds.y){
					offset.y = cameraBounds.y + (viewPort.height>>1);
					viewPort.y = cameraBounds.y;
				}else if(viewPort.y+viewPort.height>cameraBounds.height){
					offset.y = cameraBounds.height - (viewPort.height>>1);
					viewPort.y = cameraBounds.height-viewPort.height;
				}
			}
			//_touchPanel.x=viewPort.x;
			//_touchPanel.y=viewPort.y;
		}
		
		public function reset():void
		{
			for (var i:int=0;i<space.bodies.length;i++){
				_deathrow.push(space.bodies.at(i));
			}
			cleanUp();
			space.clear();
			removeChild(_touchPanel,true);
			this.removeChildren();
		}

	}
}