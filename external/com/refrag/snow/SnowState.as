package com.refrag.snow
{
	import starling.animation.Juggler;
	import starling.display.Sprite;
	
	public class SnowState extends Sprite
	{
		
		public var juggler:Juggler;
		
		public function SnowState()
		{
			juggler = new Juggler();
			super();
		}
		
		public function add(obj:SnowObject):void
		{
			obj.juggler = juggler;
			addChild(obj.sprite);
		}
		
		public function updateFrame():void
		{
			juggler.advanceTime(1/SnowGame.TARGET_FPS);
		}
		
		//Override me to do things when I'm switched to
		public function switchedTo():void
		{
			
		}
	}
}