package com.refrag.snow.system
{
	import com.refrag.snow.SnowGame;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.EnterFrameEvent;
	/**
	 * You probably shouldn't be looking in here.
	 * This is only really used by SnowGame.
	 * 
	 * This is the container Starling Sprite for the whole damn Game 
	 */
	public class GameContainer extends Sprite
	{
		public function GameContainer()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE,init);
		}
		
		private function init(e:Event):void
		{
			addEventListener(Event.ENTER_FRAME,update);
		}
		
		private function update(e:EnterFrameEvent):void
		{
			SnowGame.update(e);
		}
	}
}