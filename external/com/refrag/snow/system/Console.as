package com.refrag.snow.system
{
	import com.refrag.snow.SnowGame;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	public class Console extends Sprite
	{
		private var _watchedObjects:Vector.<Object>;
		
		public function Console()
		{
			_watchedObjects = new Vector.<Object>;			
			super();
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function log(Data:Object):void
		{
			trace((Data == null)?"ERROR: null object":Data.toString());
				
		}
		
		public function watch(name:String, o:Object):void{
			_watchedObjects.push(o);
		
		}
			
		
		private function init(e:Event):void
		{
			addChild(new Quad(SnowGame.width,50,0x66666666,true));
			addChild(new TextField(SnowGame.width,50,"Hello Console","Verdana",12));
		}
	}
}