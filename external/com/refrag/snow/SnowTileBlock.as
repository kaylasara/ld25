package com.refrag.snow
{
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	public class SnowTileBlock extends Sprite
	{
		private var _tilesWide:int;
		private var _map:Rectangle;
		public var heightInTiles:int;
		public var widthInTiles:int;
		
		private var _block:BitmapData;
		
		private var _data:Array;
		
		public function SnowTileBlock()
		{
			_map = new Rectangle();
			super();
			this.touchable=false;

		}
		
		public function loadMapfromTexture(s:String, t:Vector.<Texture>, tileHeight:int, tileWidth:int):void
		{
			_map.x=0;
			_map.y=0;
			_map.width=s.length*tileWidth;
			_map.height=tileHeight;
			
			var c:int;
			var cols:Array;
			var rows:Array = s.split("\n");
			heightInTiles = rows.length;
			_data = new Array();
			
			for (var r:int = 0; r<heightInTiles;r++)
			{
				cols = rows[r].split(",");
				if(cols.length <=1)
				{
					heightInTiles--;
					continue;
				}
				if(widthInTiles == 0)
					widthInTiles = cols.length;
				for(c=0;c<widthInTiles;c++)
					_data.push(int(cols[c]));
			}
			
			var pointer:int = 0;
			for (var j:int = 0;j<heightInTiles;j++){
				for (var i:int = 0;i<widthInTiles;i++)
				{
					var img:Image = new Image(t[_data[pointer]]);
					img.x = i*tileWidth;
					img.y= j*tileHeight;
					img.smoothing = TextureSmoothing.NONE;
					img.blendMode = BlendMode.NONE;

					this.addChild(img);
					pointer++;
				}
			
			}	
			//this.flatten(); -- Flattening is expensive. Do this higher up as little as possible.
		}
		
		public function loadMapfromBitmap(s:String, b:Bitmap, tileHeight:int, tileWidth:int):void
		{
			_map.x=0;
			_map.y=0;
			_map.width=s.length*tileWidth;
			_map.height=tileHeight;
			
			var c:int;
			var cols:Array;
			var rows:Array = s.split("\n");
			heightInTiles = rows.length;
			_data = new Array();
			
			for (var r:int = 0; r<heightInTiles;r++)
			{
				cols = rows[r].split(",");
				if(cols.length <=1)
				{
					heightInTiles--;
					continue;
				}
				if(widthInTiles == 0)
					widthInTiles = cols.length;
				for(c=0;c<widthInTiles;c++)
					_data.push(int(cols[c]));
			}
			
			_block = new BitmapData(widthInTiles*tileWidth,heightInTiles*tileHeight,true,0xFFFFFFFF);
			
			var _source:BitmapData = b.bitmapData;
			
			var pointer:int = 0;
			for (var j:int = 0;j<heightInTiles;j++){
				for (var i:int = 0;i<widthInTiles;i++)
				{
					_block.copyPixels(_source,new Rectangle(_data[pointer]*tileWidth,0,tileWidth,tileHeight),new Point(i*tileWidth,j*tileHeight));
					pointer++;
				}
				
			}	
			var t:Texture = Texture.fromBitmapData(_block,false,false,1);
			var img:Image = new Image(t);
			img.smoothing = TextureSmoothing.NONE;
			//img.blendMode = BlendMode.NONE;
			this.addChild(img)
		}
	}
}