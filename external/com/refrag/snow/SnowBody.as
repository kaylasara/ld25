package com.refrag.snow
{	
	import flash.geom.Rectangle;
	
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.shape.Circle;
	import nape.shape.Polygon;
	
	public class SnowBody
	{
		public static const TYPE_HERO:String = "TYPE_HERO";
		public static const TYPE_ENEMY:String = "TYPE_ENEMY";
		public static const TYPE_NPC:String = "TYPE_NPC";
		public static const TYPE_WORLD:String = "TYPE_WORLD";
		
		public static const DYNAMIC:CbType = new CbType();
		public static const WORLD:CbType = new CbType();
		public static const BLOCK:CbType = new CbType();
		public static const ENEMY:CbType = new CbType();
		public static const HERO:CbType = new CbType();
		
		public static const HERO_FILTER:InteractionFilter = new InteractionFilter(4,1);
		public static const ENEMY_FILTER:InteractionFilter = new InteractionFilter(3,1);
		public static const NOCOLLIDE_FILTER:InteractionFilter = new InteractionFilter(2,0);
		public static const NPC_FILTER:InteractionFilter = new InteractionFilter(5,1);
		
		public var napeBody:Body;
		public var type:String;
		private var napeFilter:InteractionFilter;
		private var napeCBType:CbType;
		private var _collides:Boolean;
		private var _collision:String;
		private var _interaction:String;
		
		
		
		public function SnowBody(group:String = SnowBody.TYPE_WORLD)
		{
			type = group;
			
			if (type == SnowBody.TYPE_WORLD){
				napeBody = new Body(BodyType.STATIC, new Vec2(0,0));
				napeBody.cbTypes.add(SnowBody.WORLD);
			}
			else{
				napeBody = new Body(BodyType.DYNAMIC, new Vec2(0,0));
				napeBody.cbTypes.add(SnowBody.DYNAMIC);
			}
		}
		
		public function addType(cbtype:CbType):void
		{
			napeBody.cbTypes.add(cbtype);
		}
		
		public function getFilter():InteractionFilter
		{
			var f:InteractionFilter;
			
			if (type == SnowBody.TYPE_HERO) f = SnowBody.HERO_FILTER;
			else if (type == SnowBody.TYPE_ENEMY) f = SnowBody.ENEMY_FILTER;
			else if (type == SnowBody.TYPE_NPC) f = SnowBody.NPC_FILTER;
			else if (type == SnowBody.TYPE_WORLD) f = null;
				
			return f;
		}
		
		public function addCircle(radius:Number):void
		{
			napeBody.shapes.add(new Circle(radius,null,null,getFilter()));
		}
		
		public function addRectangle(rect:Rectangle):void
		{
			napeBody.shapes.add(new Polygon(Polygon.rect(rect.x,rect.y,rect.width,rect.height)));
		}

		public function get collides():Boolean
		{
			return _collides;
		}

		public function set collides(value:Boolean):void
		{
			_collides = value;
			if (!_collides) 
				napeBody.shapes.at(0).filter = SnowBody.NOCOLLIDE_FILTER;
			else
				napeBody.shapes.at(0).filter = getFilter();
				
		}

		public function get collision():String
		{
			return _collision;
		}

		public function set collision(value:String):void
		{
			_collision = value;
		}

		public function get interaction():String
		{
			return _interaction;
		}

		public function set interaction(value:String):void
		{
			_interaction = value;
		}

		
	}
}