package com.refrag.snow
{
	public class SnowPoint
	{
		public var x:Number;
		private var _y:Number;
		
		public function SnowPoint()
		{
		}

		public function get y():Number
		{
			return _y;
		}

		public function set y(value:Number):void
		{
			_y = value;
		}

	}
}