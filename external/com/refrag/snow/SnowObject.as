package com.refrag.snow
{
	
	import starling.animation.Juggler;
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class SnowObject
	{
		public var juggler:Juggler;
		public var x:Number;
		public var y:Number;
		public var width:Number;
		public var height:Number;
		
		/**
		 * Our actual Starling Graphic
		 */
		public var sprite:Sprite;
		
		public function SnowObject()
		{
			super();
		}
		
		/**
		 * Sets an image for this object using an already existing TextureAtlas.
		 * This can improve draw calls and performance.
		 * 
		 * @param atlas The SnowAtlas object
		 * @param name The String that the texture would start with
		 */
		public function setImageFromAtlas(atlas:SnowAtlas,name:String):void			
		{
			sprite = new Sprite();
			var img:Image = new Image(atlas.getTexture(name));
			sprite.addChild(img);

		}
		
	}
}