package com.refrag.snow.dialog
{
	import com.refrag.snow.SnowGame;
	import com.refrag.snow.SnowSound;
	
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;

	/*
	I'm just a cute little Class that likes to display messages.
	I'll queue them for you! And make them wait their turn.
	Heck, I'll even run a function when I'm done if you'd like.
	But only give me one function. I'll ignore older ones.
	
	I like Starling and long Strings that I can splice.
	*/
	public class TextPanel extends Sprite
	{
		[Embed(source="type1.mp3") ] private static var typeSound1_data:Class;
		[Embed(source="type2.mp3") ] private static var typeSound2_data:Class;
		[Embed(source="type3.mp3") ] private static var typeSound3_data:Class;
		[Embed(source="type4.mp3") ] private static var typeSound4_data:Class;
		[Embed(source="carriageReturn.mp3") ] private static var returnSound_data:Class;
		
		private var txt:TextField;
		
		private var messages:Vector.<String>;
		private var currentMessage:String;
		private var currentPosition:int;
		private var displayMessage:String;
		
		private var lines:int = 3;
		private var currentLine:int =1;
		
		private const delay:Number = 0.005;
		private const lineDelay:Number = 0.15;
		private const messageDelay:Number = 0.5;
		
		private var wait:Number = 0;
		private var ready:Boolean = true;
		
		private var callBack:Function;
		private var called:Boolean;
		
		private var w:int;
		private var h:int;
		private var font:String;
		private var fontSize:int;
		
		private var typeSound1:SnowSound;
		private var typeSound2:SnowSound;
		private var typeSound3:SnowSound;
		private var typeSound4:SnowSound;
		
		private var returnSound:SnowSound;
		
		public function TextPanel(w:int = 0, h:int = 95, font:String = "Verdana", fontSize:int=24)
		{
			typeSound1 = new SnowSound();
			typeSound1.loadEmbedded(typeSound1_data);
			
			typeSound2 = new SnowSound();
			typeSound2.loadEmbedded(typeSound2_data);
			
			typeSound3 = new SnowSound();
			typeSound3.loadEmbedded(typeSound3_data);
			
			typeSound4 = new SnowSound();
			typeSound4.loadEmbedded(typeSound4_data);
			
			returnSound = new SnowSound();
			returnSound.loadEmbedded(returnSound_data);
			
			this.w = w;
			this.h=h;
			this.font = font;
			this.fontSize = fontSize;
			lines = Math.floor(h/(fontSize * 1.25));
			trace ("Dialog Lines: " + lines);
			currentMessage = "";
			messages = new Vector.<String>;
			currentPosition = 0;
			super();
			addEventListener(Event.ADDED_TO_STAGE,init);
		}
		
		private function init(e:Event):void
		{
			if (w==0) w = SnowGame.width-200;

			txt = new TextField(w,h,"",font,fontSize)
			addChild(txt);

			txt.hAlign = HAlign.LEFT;
			txt.vAlign = VAlign.TOP;
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		private function update(e:EnterFrameEvent):void
		{
			wait +=e.passedTime;

			if (currentPosition==currentMessage.length && messages.length>0){

				currentMessage = messages.shift();
				currentPosition =0;
				currentLine =0;
				
				if (!ready) wait = -messageDelay;
			}
				
			else if (currentPosition == currentMessage.length){
				//I'm at the end of a message and there are no further messages.
				//I'm definitely at a pause!
				ready = true;
				
				if (!called){
					called=true;
					trace("Calling");
					if (callBack) callBack.call();
					wait -= messageDelay;
				}

				return;
			}
				
				
			
			if (wait>delay){
				ready = false;
				if (currentPosition == 0)
					displayMessage = "";
				wait = 0;
				var char:String;
				char = currentMessage.substr(currentPosition, 1);
				playType();
				if (char=="\n"){
					
					currentLine++;
					if (currentLine>lines){
						var strings:Array;
						strings = displayMessage.split("\n");
						displayMessage = "";
						for (var i:int =1; i<strings.length;i++)
						{
							displayMessage +=strings[i];
							displayMessage += "\n";
							returnSound.play();
						}
					displayMessage = displayMessage.substr(0,displayMessage.length-1);
					currentLine--;	
					}
					
				}
				displayMessage += char;
				currentPosition++;
				//Extra delay if the next character is a line break
				if (currentMessage.substr(currentPosition,1)=="\n")
					wait = -lineDelay;
			}
			txt.text = displayMessage;
		}
		
		private function playType():void
		{
			typeSound1.play();
		}
		
		public function addMessage(s:String, callBck:Function = null):void
		{
			messages.push(s);
			if (currentMessage == "")
				currentMessage = messages.shift();
		
			this.callBack = callBck;
			called=false;
			if (callBck == null){
				trace("It's null");
			}else{
				trace(callBack);
			}
		}
	}
}