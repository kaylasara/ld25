package com.refrag.snow.dialog
{
	import flash.utils.ByteArray;
	
	//The convo manager handles the XML importing and generates discreet conversations to handle each xml file
	public class ChatManager
	{
		private static var convoTable:Array;
		
		private static var xml:XML = new XML();
		
		public static var actors:Array;
		
		public static var vars:Object;
		
		public function ChatManager()
		{
		}
		
		public static function initialize (b:ByteArray):void
		{
			convoTable = new Array();
			actors = new Array();
			xml = byteArray2XML(b);
			setupActors();
			
			setupVariables();
			//trace(verifyCondition("Test"));

			var tmpXML:XML = xml.Assets..Conversation[0];
			convoTable.push(new Conversation(tmpXML));
			
		}

		public static function byteArray2XML(b:ByteArray):XML
		{
			var ba:ByteArray = b;
			
			return new XML(ba.readUTFBytes(ba.length));
			
		}
		
		private static function setupActors():void
		{
			//Create a blank actor at ID 0 since Chat Mapper starts IDs at "1"
			actors.push("Sir Blank-a-lot");
			
			//Populate the array with actor names
			var i:int=0;		
			for each (var e:XML in xml..Actor){
				actors.push(e..Field.(Title=="Name").Value.toString());
				i++;
				
			}
			trace(actors);
		}

		private static function setupVariables():void
		{
			vars = new Object();
			//Populate our vars Object with the XML variables
			var i:int=0;		
			for each (var e:XML in xml..UserVariable){
				vars[(e..Field.(Title=="Name").Value.toString())] = (e..Field.(Title=="Initial Value").Value);
				i++;
			}
		}
		
		public static function verifyCondition(s:String):Boolean{
			trace("Verifying Condition... " + s);
			//s = "Variable[\"variable1\"]==2";
			var conditions:Array = s.split("\n");
			var bool:Boolean =  true;
			
			for (var i:int = 0; i<conditions.length;i++){
				trace ("Condition #" + i + " = " + conditions[i]);
				var type:String = conditions[i].substr(0,3);
				var object:String = conditions[i].split("\"")[1];
				var comparison:String = conditions[i].split("]")[1];
				trace (type + object + comparison);
				if (type =="Var"){
					trace("Variable type!")
					return compare(vars[object],comparison); 
				}else{
					return true; //Fuck it, I don't know what you're asking me
				}
				
			}
			
			
			return bool;
			
		}
		
		private static function compare(val1:Object, condition:String):Boolean
		{
			var type:String = condition.substr(0,2);
			var val2:String = condition.substr(2,condition.length)
			if (type == "==")
				return val1 == val2;
			else if (type == "!=")
				return val1 != val2;
			else if (type = "<=")
				return val1 <= val2;
			else if (type = ">=")
				return val1 >= val2;
			else
				return true; // Fuck it, I don't know what you're asking me.
		}
		
		public static function getActor(i:int):String
		{
			return (actors[i]);
		}
		
		public static function getCurrentConvo():Conversation
		{
			return(convoTable[0]);
		}
		
	}
}