package
{
	import com.refrag.snow.SnowGame;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class StarBattleEnter extends Sprite
	{
		
		[Embed(source = "../data/battleStar2.png")] private static const Star1Bitmap:Class;
		[Embed(source = "../data/battleStar2.png")] private static const Star2Bitmap:Class;
		[Embed(source = "../data/battleStar1.png")] private static const Star3Bitmap:Class;
		

		private var star1Tex:Texture;
		private var star2Tex:Texture;
		private var star3Tex:Texture;
		

		private var star1:Image;
		private var star2:Image;
		private var star3:Image;
		
		
		private var callback:Function;
		
		private var timer:Number;
		private var maxTime:Number;
		
		public function StarBattleEnter()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void
		{
			star1Tex = Texture.fromBitmap(new Star1Bitmap());
			star2Tex = Texture.fromBitmap(new Star2Bitmap());
			star3Tex = Texture.fromBitmap(new Star3Bitmap());
			
			star1 = new Image(star1Tex);
			star1.pivotX = star1.width >>1;
			star1.pivotY = star1.height >>1;
			
			star1.scaleX = 1.5;
			star1.scaleY = 1.5;
			star1.x = SnowGame.width /2;
			star1.y = SnowGame.height/2;
			star1.touchable = false;
			star1.alpha = 1;
			
			
			
			star3 = new Image(star3Tex);
			star3.pivotX = star3.width >>1;
			star3.pivotY = star3.height >>1;
			
			star3.scaleX = 3.5;
			star3.scaleY = 3.5;
			star3.x = SnowGame.width /2;
			star3.y = SnowGame.height/2;
			star3.touchable = false;
			star3.alpha = 1;
			
			addChild(star3);
			this.visible = false;
			
			star2 = new Image(star2Tex);
			star2.pivotX = star2.width >>1;
			star2.pivotY = star2.height >>1;
			
			star2.scaleX = 1;
			star2.scaleY = 1;
			star2.x = SnowGame.width /2;
			star2.y = SnowGame.height/2;
			star2.touchable = false;
			star2.alpha = 0.25;
			
			addChild(star2);
			addChild(star1);
			
			//addChild(winText);
			this.visible = false;
		}
		
		public function start(t:Number = 10, callback:Function=null):void
		{
			this.visible = true;
			this.callback = callback;
			maxTime = t;
			timer = 0;
			addEventListener(Event.ENTER_FRAME,loop);
			
		}
		
		private function loop(e:EnterFrameEvent):void
		{
			timer += e.passedTime;
			star1.scaleX = 0.5 + (timer/maxTime)*5.5;
			star1.scaleY = 0.5 + (timer/maxTime)*5.5;
			
			//star2.rotation = timer/maxTime * 10;
			star3.rotation = -(timer/maxTime * 2);
			star3.alpha = 0 + (timer/maxTime);
			if (timer>maxTime){
				removeEventListener(Event.ENTER_FRAME,loop);
				this.visible = false;
				callback.call();
				star1.scaleX = 1.5;
				star1.scaleY = 1.5;
			}
		}
	}
}