package
{
	import com.refrag.snow.SnowGame;
	import com.refrag.snow.SnowSound;
	import com.refrag.snow.SnowState;
	
	import feathers.controls.Button;
	import feathers.themes.MinimalMobileTheme;
	
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	import starling.textures.Texture;
	
	public class EndState extends SnowState
	{
		
		private var theme:MinimalMobileTheme;
		
		public function EndState()
		{
			GameVars.points =1;
			GameVars.blocked=true;
			GameVars.aggression=1;
			GameVars.skill=1;
			GameVars.wit=1;
			super();
			addEventListener(Event.ADDED_TO_STAGE,init);
			
		}
		
		private function init(e:Event):void
		{
			theme = new MinimalMobileTheme(SnowGame.stage);
			var aggressionButton:Button = new Button();
			aggressionButton.label = "Play Game";
			aggressionButton.x = SnowGame.width/2 - 75
			aggressionButton.y = SnowGame.height -200;
			aggressionButton.onRelease.add(selected);
			
			var t1:TextField = new TextField(SnowGame.width,400,"The Evil Machinations\nof\nFluffy McSnugglebottom, Carnivore","Verdana",42,0xFFFFFFFF,true);
			t1.hAlign = HAlign.CENTER;
			t1.vAlign = VAlign.TOP;
			t1.y = 100;
			addChild(t1);
			
			var t2:TextField = new TextField(SnowGame.width,400,"Game Over","Verdana",22,0xFFFFFFFF,true);
			t2.hAlign = HAlign.CENTER;
			t2.vAlign = VAlign.TOP;
			t2.y = 300;
			addChild(t2);
			
			//addChild(aggressionButton);
			
		}
		
		private function selected(b:Button):void
		{
			SnowGame.switchState(new PlayState());		
		}
	}
}