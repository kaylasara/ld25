package
{
	import com.refrag.snow.Utils.Device;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
		
	import starling.core.Starling;
	
	[SWF(frameRate="30", width="960", height="620", backgroundColor="0x333333")]
	public class RunAndroid extends Sprite
	{
		public function RunAndroid()
		{
			Starling.handleLostContext = true;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;


				var star:Starling = new Starling(StartState, stage,new Rectangle(0,0,Capabilities.screenResolutionX,Capabilities.screenResolutionY));
			
			star.start();
		}
		
		
	}
}