package
{
	public class Ground1 extends Ground
	{
		[Embed(source = '../data/block1.csv', mimeType = 'application/octet-stream')] private var BlockCSV:Class;
		
		[Embed(source = "../data/ground.png")] private static const GroundBitmap:Class;


		public function Ground1()
		{
			setup(new BlockCSV(),new GroundBitmap());
			super();
		}
	}
}