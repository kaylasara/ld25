package 
{
	import com.refrag.snow.SnowBody;
	import com.refrag.snow.SnowCharacter;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.utils.ByteArray;
	
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	public class NPC3 extends SnowCharacter
	{
		[Embed( source="../data/cat3.xml", mimeType="application/octet-stream" )] private static var Convo1XML:Class;
		[Embed(source = "../data/npc1.png")] private const SpriteSheet:Class;

		public function NPC3()
		{
			var body:SnowBody = new SnowBody(SnowBody.TYPE_NPC);
			body.addCircle(32);
			this.addBody(body);
			super();
			sprite.addEventListener(Event.ADDED_TO_STAGE,init);
		}
		
		private function init(e:Event):void
		{
			this.setConversation(new Convo1XML() as ByteArray)
			
			var bitmap:Bitmap = new Bitmap(new BitmapData(32,96,true, 0xFF999999));
			
			setImage(new SpriteSheet());
			this.image.y=this.image.y-115;
			this.image.scaleX =2;
			this.image.scaleY =2;
			this.image.smoothing = TextureSmoothing.NONE;
			this.velocityY = -50;
		}
		
		public function jump():void
		{
			
		}
		
		override public function moveLeft():void
		{
			velocityX -=50;
		}
		
		override public function moveRight():void
		{
			velocityX +=50;
		}
		
		public function isDead():Boolean{
			if (y>1000) {
				return false;
			}else
				return false;
			
			
		}
	}
}