package
{
	import com.refrag.snow.Utils.Device;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.geom.Rectangle;
	
	
	import starling.core.Starling;
	
	[SWF(frameRate="60", width="960", height="620", backgroundColor="0x9785a6")]
	public class RunIOS extends Sprite
	{
		public function RunIOS()
		{

			Starling.handleLostContext = true;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;

			var star:Starling = new Starling(StartState, stage,new Rectangle(0,0,960,640));
			
			star.start();
			
			
			if (Device.getDevice() == Device.IPHONE_4)
				stage.frameRate = 45;
			else 
				stage.frameRate = 60;
			
			
			
			
		}
		
		
	}
}