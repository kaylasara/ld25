package 
{
	import com.refrag.snow.SnowGame;
	import com.refrag.snow.SnowSound;
	import com.refrag.snow.dialog.Conversation;
	import com.refrag.snow.dialog.TextPanel;
	
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import com.refrag.snow.SnowPhysSprite;

	public class ChatWindow
	{
		[Embed(source="../data/mew.mp3") ] private static var mewSound_data:Class;
		[Embed(source="../data/click.mp3") ] private static var clickSound_data:Class;

		
		private static var mewSound:SnowSound;
		private static var clickSound:SnowSound;

		public static var sprite:Sprite = new Sprite();
		private static var textPanel:TextPanel;
		
		private static var name:TextField;
		private static var notification:TextField;
		
		private static var replyText:Vector.<TextField>;
		private static var replyPanels:Array;

		private static var conv:Conversation;
		
		private static var head:Image;
		private static var callBack:Function;
		
		public static var chatGame:ChatGame;
		
		private static var character:Quad;
		
		private static var result:Boolean = false;
		
		public function ChatWindow()
		{
			
		}
		
		public static function initialize():void
		{
			if (textPanel!=null)
				return
			mewSound = new SnowSound();
			mewSound.loadEmbedded(mewSound_data);
			
			clickSound = new SnowSound();
			clickSound.loadEmbedded(clickSound_data);
			
			textPanel = new TextPanel(760,156);
			replyPanels = new Array();
			replyText = new Vector.<TextField>;
			//Reply selections
			for (var i:int =0; i<3;i++){
				var s:Sprite = new Sprite()
				var q:Quad = new Quad(960,90,0xFFFFFF);
				q.alpha = 0.9;
				
				s.addChild(q);
				
				var t:TextField = new TextField(960,90,"Hello","Verdana",24);
				t.touchable = false;
				s.addChild(t);
				replyText.push(t);
				
				s.y = -425 + 110*i;
				s.addEventListener(TouchEvent.TOUCH, replyAction);
				replyPanels.push(s);
				sprite.addChild(s);

			}
			//Background
			var q:Quad = new Quad(SnowGame.width,156,0xFFFFFF);
			q.alpha = 0.75;
			q.blendMode = BlendMode.SCREEN;			
			sprite.addChild(q);
			
			var q2:Quad = new Quad(SnowGame.width,156,0xFFFFFF);
			q2.alpha = 0.5;
			q2.blendMode = BlendMode.SCREEN;
			q2.rotation = 0.04;
			q2.y -=35;
			sprite.addChild(q2);
			
			//Name Plate
			name = new TextField(250,30,"Actor","Verdana",24);
			name.x -=25;
			name.y-=30;
			sprite.addChild(name);
			
			notification = new TextField(100,32,"...","Verdana",12,0x000000,true);
			notification.x = SnowGame.width - 100;
			notification.y +=64;
			sprite.addChild(notification);
			
			//Text Panel
			sprite.addChild(textPanel);
			
			textPanel.x += 100;
			
			//Head Image	
			//var glowTex:Texture = Texture.fromBitmap(new HeadBitmap());
			/*head = new Image(glowTex);
			head.scaleX = 0.5;
			head.scaleY = 0.5;
			head.x = SnowGame.width - 128;
			head.y -=128;
				*/
			//Full Chat Window placement	
			//sprite.addChild(head);
			sprite.y = SnowGame.height - 160;
			sprite.visible = false;

			chatGame = new ChatGame();

		}
		
		public static function simpleMessage(s:String, func:Function):void
		{
			conv = null;
			result = false;
			for (var i:int =0;i<replyPanels.length;i++) replyPanels[i].visible = false;

			notification.visible = true;
			sprite.visible = true;
			callBack = func;

			name.text = "";
			textPanel.addMessage(s,waitSimple);
		}
		
		public static function message(c:Conversation, func:Function, ch:Quad):void
		{
			result = false;
			character = ch;
			callBack = func;
			sprite.visible = true;
			conv = c;
			conv.gotoID(1);
			name.text = conv.actor;
			textPanel.addMessage("Starting conversation!");
			trace("It's this:" + conv.replyIDs.length);
			nextMessage();			
		}
		
		private static function nextMessage():void
		{
			if (!sprite.visible) sprite.visible = true;
			notification.visible = false;
			for (var i:int =0;i<replyPanels.length;i++) replyPanels[i].visible = false;
			
				name.text = conv.actor;
				textPanel.addMessage("> " + conv.text, waitForAction);
				
		}
		
		private static function waitForAction():void
		{
			if (conv.replyIDs.length >1){
				
				for (var i:int =0; i<conv.replyIDs.length;i++){
					replyText[i].text = checkReply(conv.replyText[i]);
					replyPanels[i].visible = true;
						
				}
			} else{
				sprite.addEventListener(TouchEvent.TOUCH, continueAction);
				notification.visible = true;
				notification.text = "continue...";
			}
			
		}
		
		private static function checkReply(s:String):String
		{
			var txt:String;
			var difficulty:Number;
			
			if (s=="AGGRESSIVE"){
				difficulty = (100-Math.floor((GameVars.aggression/((GameVars.points+1)*GameVars.points)) * 100));
				if (difficulty<0) difficulty=0;
				txt = "AGGRESSIVE RESPONSE [Difficulty " + difficulty+ "]";	
			}
			 
			else if (s=="WITTY")
				txt = "SEDUCTIVE RESPONSE [Difficulty " + (100-Math.floor((GameVars.wit/((GameVars.points+1)*GameVars.points)) * 100)) + "]"; 
			else if (s=="SKILLFUL")
				txt = "INTELLECTUAL RESPONSE [Difficulty " + (100-Math.floor((GameVars.skill/((GameVars.points+1)*GameVars.points)) * 100)) + "]"; 
			else
				txt = s;
			return txt;
			
		}
		private static function waitSimple():void
		{
			sprite.addEventListener(TouchEvent.TOUCH, continueAction);
			notification.visible = true;
			notification.text = "continue...";
		}
		
		private static function finishUp():void
		{
			sprite.visible = false;
			//chatGame.start(gameEnd);
			callBack.call(null,character,result);
			
		}
		
		private static function gameEnd(r:Boolean):void
		{
			result=r;
			trace("END: " + r);
			SnowGame.flash(1);
			if (r) {
				GameVars.points ++;
				nextMessage();
				mewSound.play();
			}else{
				finishUp();
			}
			
		}
		
		private static function continueAction(e:TouchEvent):void
		{
			if (e.touches.length > 0){
				var touch:Touch = e.touches[0];
				if (touch.phase == TouchPhase.ENDED)
				{
					sprite.removeEventListener(TouchEvent.TOUCH,continueAction);
					if (!conv) {
						finishUp();
						return;
					}

					if (conv.replyIDs[0]){
						conv.gotoID(conv.replyIDs[0]);
						nextMessage();
					}else finishUp();
						

					
				}
			}
		}
		
		private static function replyAction(e:TouchEvent):void
		{
			
			if (e.touches.length > 0)
			{
				var touch:Touch = e.touches[0];

				
				if (touch.phase == TouchPhase.ENDED)
				{
					clickSound.play();
					if (conv.replyText[replyPanels.indexOf(touch.target.parent)] == "AGGRESSIVE"){
						sprite.visible = false;
						conv.gotoID(conv.replyIDs[replyPanels.indexOf(touch.target.parent)]);
						chatGame.start("You fume and attempt an aggressive response.",10 - Math.floor((GameVars.aggression/((GameVars.points+1)*GameVars.points)) * 10), gameEnd);
						return;
					}
					else if (conv.replyText[replyPanels.indexOf(touch.target.parent)] == "WITTY"){
						sprite.visible = false;
						conv.gotoID(conv.replyIDs[replyPanels.indexOf(touch.target.parent)]);
						chatGame.start("You attempt to charm the foe and ready a seductive response.",10 - Math.floor((GameVars.wit/((GameVars.points+1)*GameVars.points)) * 10), gameEnd);
						return;
					}
					else if (conv.replyText[replyPanels.indexOf(touch.target.parent)] == "SKILLFUL"){
						sprite.visible = false;
						conv.gotoID(conv.replyIDs[replyPanels.indexOf(touch.target.parent)]);
						chatGame.start("You attempt a response that utilizes your superior intellect.",10 - Math.floor((GameVars.skill/((GameVars.points+1)*GameVars.points)) * 10), gameEnd);
						return;
					}
					trace("going to..." +touch.target.parent);
					conv.gotoID(conv.replyIDs[replyPanels.indexOf(touch.target.parent)]);
					nextMessage();
				}
			}
		}
		
	}
}