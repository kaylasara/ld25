package
{
	import com.refrag.snow.SnowGame;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class StartState extends Sprite
	{
		public function StartState()
		{
			super();
			
			addEventListener(Event.ADDED_TO_STAGE,init);
		}
		
		private function init(e:Event):void
		{
			
			SnowGame.start(stage);
			SnowGame.switchState(new MenuState());
		}
	}
}