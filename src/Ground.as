package
{
	import com.refrag.snow.SnowTileBlock;
	
	import flash.display.Bitmap;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class Ground extends Sprite
	{
		
		
		[Embed(source = "../data/ground.png")] private static const GroundBitmap:Class;

		private var tilemap:SnowTileBlock;  
		
		//private var blockString:Object = new BlockCSV();
		public var blockWidth:int;
		public var blockHeight:int;
		
		private var s:String;
		private var bitmap:Bitmap = new Bitmap();
		
		public function Ground(csv:Object)
		{

			super();
			setup(csv,new GroundBitmap());
		}
		
		public function setup(csv:Object,bitmap:Bitmap):void
		{
			this.bitmap = bitmap;
			//trace (blockString.toString());
			var s:String = csv.toString();
			var charCode:Number = 13;
			var char:String = String.fromCharCode(charCode);
			
			s = s.replace(char,"\n");
			trace(s);
			var array:Array = new Array();
			
			
			array = s.split("\n");
			for (var i:int; i<s.length;i++){
				trace(s.charCodeAt(i));
			}
			trace(array.length);
			blockHeight = array.length * 64;
			
			var array2:Array = new Array();
			array2 = array[0].split(",");
			
			blockWidth = array2.length * 64;
			
			
			trace (blockWidth + "x" + blockHeight);
			this.s = s;
			addEventListener(Event.ADDED_TO_STAGE,init);

		}
		
		private function init(e:Event):void
		{
			tilemap = new SnowTileBlock();
			tilemap.loadMapfromBitmap(s,bitmap,32,32);
			tilemap.scaleX = 2;
			tilemap.scaleY = 2;
			//tilemap.y-=32;
			addChild(tilemap);
			
			
		}
	}
}