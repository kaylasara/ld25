package 
{
	import com.refrag.snow.SnowPhysSprite;
	
	import flash.geom.Point;
		
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class TouchManager extends Sprite
	{
		
		private static var hero:Hero;
		private static var scene:PhysScene;
		
		private static var startX:Number;
		private static var startY:Number;
		private var q:Quad;
		private var delta:Point;
		
		private var touchables:Vector.<SnowPhysSprite>;
		private var touchObjects:Vector.<Quad>;
		
		public function TouchManager()
		{
		}
		
		public function init(h:Hero,s:PhysScene):void{
			touchables = new Vector.<SnowPhysSprite>;
			touchObjects = new Vector.<Quad>;
			hero = h;
			scene = s;
			q = new Quad(32,32);
			q.x = startX;
			q.y= startY;
			addChild(q);
			q.visible = false;
		}
		
		public function setTouchObject(o:SnowPhysSprite):Quad{
			touchables.push(o);
			var q:Quad = new Quad(o.sprite.width, o.sprite.height,0x000000);
			q.alpha = 0;
			touchObjects.push(q);
			return (q)
		}
		
		public function getTouchObject(q:Quad):SnowPhysSprite{
			return (touchables[touchObjects.indexOf(q)]);
		}
		
		public function updateTouchables():void{
			for (var i:int =0; i<touchables.length;i++){
				touchObjects[i].x = touchables[i].x - scene.viewPort.x;
				touchObjects[i].y = touchables[i].y - scene.viewPort.y - touchObjects[i].height/2;
			}
		}
		public function handleIt(e:TouchEvent, parent:Sprite):Boolean
		{
			var touches:Vector.<Touch> = e.getTouches(parent);
			
			
			if (touches.length > 0)
			{
				
				var touch:Touch = touches[0];
				if (touch.phase == TouchPhase.BEGAN)
				{
					q.visible = true;
					startX = touches[0].globalX;
					startY = touches[0].globalY;
					q.x = startX;
					q.y = startY;
	
					//hero.moveTo(new Vec2(touch.globalX+scene.viewPort.x,touch.globalY+scene.viewPort.y));
					
					if (touch.globalY<parent.stage.stageHeight>>1 && !hero.dead){
						
					}
						
					
					if (touch.globalY>parent.stage.stageHeight>>1 && touch.globalX<parent.stage.stageWidth>>1 && !hero.dead)
					{
						//hero.brake=true;
						//hero.moveTo(new Vec2(touch.globalX,touch.globalY));
					}
				}
				
				if (touch.phase == TouchPhase.MOVED)
				{
				
					delta = touch.getMovement(q);
					
					hero.body.velocity.x += delta.x *5;
					//if (hero.onFloor) hero.body.velocity.y += delta.y*10;
					//hero.body.ve
				}
				
				if (touch.phase == TouchPhase.HOVER)
				{
					if (touch.globalY>parent.stage.stageHeight>>1 && touch.globalX<parent.stage.stageWidth>>1 && !hero.dead)
					{
						//hero.body.velocity.x*0.5;
					}
					else if (touch.globalY>parent.stage.stageHeight>>1 && touch.globalX>parent.stage.stageWidth>>1 && !hero.dead)
					{
						//hero.body.velocity.x=hero.maxVelocity*2;
					}
					
					
				}
				if (touch.phase == TouchPhase.ENDED)
				{
					q.visible = false;
					if (hero.onFloor){
						//var delta:Point = touch.getMovement(q);
						if (delta) hero.body.velocity.y += delta.y*75;
						if (hero.body.velocity.y <-1000) hero.body.velocity.y = -1000;
					}
					//hero.brake=false;
					//hero.turbo=false;
				}
			}
			return (true);
		}
	}
}