package 
{
	import com.refrag.snow.SnowBody;
	import com.refrag.snow.SnowCharacter;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.utils.ByteArray;
	
	import feathers.display.Sprite;
	
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	import com.refrag.snow.SnowSound;
	
	public class Hero extends SnowCharacter
	{
		[Embed( source="../data/FirstTest.xml", mimeType="application/octet-stream" )] private static var Convo1XML:Class;
		
		[Embed(source = "../data/cat.png")] private const SpriteSheet:Class;
		[Embed(source="../data/jump.mp3") ] private static var jumpSound_data:Class;


		private var jumpSound:SnowSound;
		
		public function Hero()
		{
			jumpSound = new SnowSound();
			jumpSound.loadEmbedded(jumpSound_data);
			
			var body:SnowBody = new SnowBody(SnowBody.TYPE_HERO);
			body.addCircle(32);
			this.addBody(body);
			this.setConversation(new Convo1XML() as ByteArray)
			super();
			sprite.addEventListener(Event.ADDED_TO_STAGE,init);
		}
		
		private function init(e:Event):void
		{
			
			var bitmap:Bitmap = new Bitmap(new BitmapData(32,96,true, 0xFF999999));
					
			setImage(new SpriteSheet());
			image.smoothing = TextureSmoothing.NONE;;
		 	image.y=this.image.y-32;
			image.pivotX = image.width/2;
			image.scaleX = 2;
			image.scaleY = 2;
		}
		
		public function jump():void
		{
			
			if (this.onFloor){
				velocityY = -500;
				jumpSound.play();
			}
		}
		
		override public function moveLeft():void
		{
			velocityX -=30;
			if (velocityX<0)
				image.scaleX = -2;
		}
		
		override public function moveRight():void
		{
			velocityX +=30;
			if (velocityX>0)
				image.scaleX = 2;
		}
		
		public function isDead():Boolean{
			if (y>1000) {
				return false;
			}else
				return false;
			
			
		}
	}
}