package 
{
	import com.refrag.snow.SnowGame;
	import com.refrag.snow.SnowSound;
	
	import battle.TextPanel;
	
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	
	public class ChatGame extends Sprite
	{
		[Embed(source = "../data/circle.png")] private static const CircleBitmap:Class;
		[Embed(source = "../data/circle_success.png")] private static const CircleSuccessBitmap:Class;
		[Embed(source = "../data/circle_failure.png")] private static const CircleFailureBitmap:Class;
		
		[Embed(source="../data/snd1.mp3") ] private static var snd1_data:Class;
		private var snd1:SnowSound;

		[Embed(source="../data/snd2.mp3") ] private static var snd2_data:Class;
		private var snd2:SnowSound;
		
		[Embed(source="../data/snd3.mp3") ] private static var snd3_data:Class;
		private var snd3:SnowSound;
		
		[Embed(source="../data/snd4.mp3") ] private static var snd4_data:Class;
		private var snd4:SnowSound;
		
		[Embed(source="../data/snd5.mp3") ] private static var snd5_data:Class;
		private var snd5:SnowSound;
		
		[Embed(source="../data/snd6.mp3") ] private static var snd6_data:Class;
		private var snd6:SnowSound;
		
		[Embed(source="../data/snd7.mp3") ] private static var snd7_data:Class;
		private var snd7:SnowSound;
		
		[Embed(source="../data/snd8.mp3") ] private static var snd8_data:Class;
		private var snd8:SnowSound;
		
		private var circleTex:Texture;
		private var circleSuccessTex:Texture;
		private var circleFailureTex:Texture;
		
		private var circle1s:Vector.<Image>;
		private var circle2s:Array;
		
		private var callback:Function;
		
		private var score:int;
		private var result:Boolean;
		
		public var difficulty:Number = 1;
		private var starWin:StarWin;
		private var textPanel:TextPanel;
		
		private var dotPanel:Sprite;
		private var snds:Array;

		
		public function ChatGame()
		{
			snds=new Array();
			snd1 = new SnowSound();
			snd1.loadEmbedded(snd1_data);
			snds.push(snd1);
			
			snd2 = new SnowSound();
			snd2.loadEmbedded(snd2_data);
			snds.push(snd2);
			
			snd3 = new SnowSound();
			snd3.loadEmbedded(snd3_data);
			snds.push(snd3);
			
			snd4 = new SnowSound();
			snd4.loadEmbedded(snd4_data);
			snds.push(snd4);
			
			snd5 = new SnowSound();
			snd5.loadEmbedded(snd5_data);
			snds.push(snd5);
			
			snd6 = new SnowSound();
			snd6.loadEmbedded(snd6_data);
			snds.push(snd6);
			
			snd7 = new SnowSound();
			snd7.loadEmbedded(snd7_data);
			snds.push(snd7);
			
			snd8 = new SnowSound();
			snd8.loadEmbedded(snd8_data);
			snds.push(snd8);
			
			
			super();
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		
		public function init(e:Event):void
		{
			dotPanel = new Sprite();
			circleTex = Texture.fromBitmap(new CircleBitmap());
			circleSuccessTex = Texture.fromBitmap(new CircleSuccessBitmap());
			circleFailureTex = Texture.fromBitmap(new CircleFailureBitmap());
			starWin = new StarWin();
			addChild(starWin);
			
			//Background
			var q:Quad = new Quad(SnowGame.width,96,0xFFFFFF);
			q.alpha = 1;
			//q.y +=35;
			q.blendMode = BlendMode.SCREEN;			
			addChild(q);
			
			var q2:Quad = new Quad(SnowGame.width,96,0xFFFFFF);
			q2.alpha = 0.5;
			q2.blendMode = BlendMode.SCREEN;
			q2.rotation = 0.04;
			q2.y -=35;
			addChild(q2);
			
			addChild(textPanel = new TextPanel());
			textPanel.x +=25;
			textPanel.y +=5;
			
			this.visible = false;
			
			addChild(dotPanel);
			
		}
		
		public function start(s:String="", difficulty:int = 10, callback:Function=null):void{
			this.visible = true;
			textPanel.addMessage(s);
			textPanel.addMessage("Begin in..");
			textPanel.addMessage("3..");
			textPanel.addMessage("2..");
			textPanel.addMessage("1..");
			textPanel.addMessage("GO!", go);
			this.difficulty = difficulty/3;
			if (this.difficulty<0.75) this.difficulty = 0.75;
			
			this.callback = callback;
		}
		
		private function go():void
		{
			score =0;
			circle1s = new Vector.<Image>;
			circle2s = new Array;
			
			newCircle();
			addEventListener(Event.ENTER_FRAME,update);
			
			
		}
		
		private function newCircle():void
		{
			var circX:int = 50 + Math.floor(Math.random()*(SnowGame.width-100));
			var circY:int = 100 + Math.floor(Math.random()*(SnowGame.height-150));
			
			var circle1:Image = new Image(circleTex);
			circle1.pivotX = circle1.width >>1;
			circle1.pivotY = circle1.height >>1;
			
			circle1.scaleX = 0.5;
			circle1.scaleY = 0.5;
			circle1.x = circX 
			circle1.y = circY;
			circle1.touchable = false;
			
			dotPanel.addChild(circle1);
			circle1s.push(circle1);
			
			var circle2:Image = new Image(circleTex);
			circle2.pivotX = circle2.width >>1;
			circle2.pivotY = circle2.height >>1;
			
			circle2.scaleX = 2;
			circle2.scaleY = 2;
			circle2.alpha = 0.5
			circle2.x = circX 
			circle2.y = circY
			circle2.addEventListener(TouchEvent.TOUCH, touched);
			dotPanel.addChild(circle2);
			circle2s.push(circle2);
		}
		
		public function update(e:EnterFrameEvent):void
		{
			
			for (var i:int =0; i<circle1s.length;i++)
			{
				
				circle2s[i].scaleX -= 0.5 * e.passedTime * difficulty;
				circle2s[i].scaleY -= 0.5 * e.passedTime * difficulty;
				
				if (circle2s[i].scaleX < circle1s[i].scaleX) kill(i);
			}
			
		}
		
		private function kill(i:int):void{
			SnowGame.flash(0.5, 0xFF0000);
			circle1s[i].texture = circleFailureTex;
			//circle1s[i].visible = false;
			circle1s.splice(i,1);
			circle2s[i].visible = false;
			circle2s.splice(i,1);
			lose();
			//newCircle();
		}
		
		private function lose():void{
			textPanel.addMessage("Fluffy has failed his attempt and run away from this encounter.\nBut he will be back. This isn't over.",cleanUp);
			result = false;
			this.removeEventListener(Event.ENTER_FRAME,update);
		}
		private function win():void{
			dotPanel.removeChildren();

			textPanel.addMessage("Fluffy's attempt is succesful...\nHe clears the furballs out of his throat and readies his villainous response.");
			result = true;
			this.removeEventListener(Event.ENTER_FRAME,update);
			starWin.start(5,cleanUp);
		}
		
		private function cleanUp():void{
			dotPanel.removeChildren();

			callback.call(null,result);
			this.visible = false;
			
		}
		
		private function touched(e:TouchEvent):void
		{
			if (e.touches.length > 0){
				var touch:Touch = e.touches[0];
				if (touch.phase == TouchPhase.ENDED)
				{
					
					var i:int = circle2s.indexOf(touch.target);
					try{
					circle1s[i].texture = circleSuccessTex;
					}catch(e:RangeError){}
					circle2s.splice(i,1)
					circle1s.splice(i,1)
					touch.target.visible = false;
					
					score ++;
					snds[score].play();
					trace("Score:" + score);
					if (score>6) {
						win();
						return;
					}
					newCircle();
					SnowGame.flash(0.25, 0x00FF00);
				}
			}	
		}
	}
}