package battle
{
	public class BattleHero extends BattleCharacter
	{
		
		public var levelXP:Array;
		public var currentXP:int;
		
		
		public function BattleHero()
		{
			name = "Fluffy McSnugglebottom";
			level =1;
			levelXP = new Array(10000,200,500,1000,2000);
			currentXP = 0;
			max_health = 100;
			health = 100;
			dex = 5;
			var skill1:Object = {name:"Swat", cost:1, type:Skills.MELEE, logic:Skills.basicAttack};
			var skill2:Object = {name:"Scratch", cost:5, type:Skills.MELEE, logic:Skills.superAttack};
			var skill3:Object = {name:"Lick Self", cost:0, type:Skills.HEAL, logic:Skills.selfHeal};
			
			attacks.push(skill1);
			attacks.push(skill2);
			attacks.push(skill3);
		}
		
	}
}