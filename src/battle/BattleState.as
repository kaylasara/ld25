package battle
{
	import com.refrag.snow.SnowGame;
	import com.refrag.snow.SnowSound;
	import com.refrag.snow.SnowState;
	
	import feathers.controls.Button;
	import feathers.themes.MinimalMobileTheme;
	
	import starling.display.BlendMode;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;

	public class BattleState extends SnowState
	{
		
		[Embed(source="../../data/hero_attack.mp3") ] private static var attackSound_data:Class;
		[Embed(source="../../data/hurt.mp3") ] private static var hurtSound_data:Class;
		[Embed(source="../../data/hero_hit.mp3") ] private static var hitSound_data:Class;
		[Embed(source="../../data/health.mp3") ] private static var healthSound_data:Class;
		
		[Embed(source="../../data/battle.mp3") ] private static var battleMusic_data:Class;

		
		private var theme:MinimalMobileTheme;
		private var textPanel:TextPanel;
		
		private var hero:BattleHero;
		private var enemy:Enemy;
		
		private var healthField:TextField;
		
		private var attackButton:Button;
		private var specialButton:Button;
		private var meditateButton:Button;
		
		private var attackList:Array;
		private var attackTurn:int;
		private var attacker:BattleCharacter;
		
		private var buttons:Vector.<Button>;
		
		private var battleWin:BattleWin;
		
		private var callback:Function;
		
		private var attackSound:SnowSound;
		private var hurtSound:SnowSound;
		private var hitSound:SnowSound;
		private var healthSound:SnowSound;
		private var battleMusic:SnowSound;
		
		private var enemyList:Vector.<Enemy>;
		
		public function BattleState()
		{
			enemyList = new Vector.<Enemy>;
			
			enemyList.push(new Enemy("Lint Ball",1,1));
			enemyList.push(new Enemy("Mosquito",5,20));
			enemyList.push(new Enemy("Squeak Toy",10,9));
			enemyList.push(new Enemy("Dead Mouse",20,5));
			
			attackSound = new SnowSound();
			attackSound.loadEmbedded(attackSound_data);
			
			hurtSound = new SnowSound();
			hurtSound.loadEmbedded(hurtSound_data);

			hitSound = new SnowSound();
			hitSound.loadEmbedded(hitSound_data);

			healthSound = new SnowSound();
			healthSound.loadEmbedded(healthSound_data);
			
			battleMusic = new SnowSound();
			battleMusic.loadEmbedded(battleMusic_data);

			
			battleWin = new BattleWin();
			hero = new BattleHero();

			super();
			
			addEventListener(Event.ADDED_TO_STAGE,init);
		}
		
		private function init(e:Event):void
		{
			addChild(new BattleBackground());
			//Background
			var q:Quad = new Quad(SnowGame.width,96,0xFFFFFF);
			q.alpha = 0.75;
			//q.y +=35;
			q.blendMode = BlendMode.SCREEN;			
			addChild(q);
			
			var q2:Quad = new Quad(SnowGame.width,96,0xFFFFFF);
			q2.alpha = 0.5;
			q2.blendMode = BlendMode.SCREEN;
			q2.rotation = 0.04;
			q2.y -=35;
			addChild(q2);
			var q3:Quad = new Quad(SnowGame.width,96,0xFFFFFF);
			q3.alpha = 0.75;
			q3.y = 560;
			q3.blendMode = BlendMode.SCREEN;			
			addChild(q3);	
			addChild(healthField = new TextField(150,30,"HEALTH " + hero.health,"Verdana",22))
			
			healthField.x = SnowGame.width/2-75;
			healthField.y = 565;
			healthField.hAlign = HAlign.RIGHT;
			
			theme = new MinimalMobileTheme(SnowGame.stage,false);

			var quitButton:Button = new Button();
			quitButton.label = "Run";
			quitButton.x = 0;
			quitButton.y = SnowGame.height-150;
			//addChild(quitButton);
			
			quitButton.onRelease.add(closePanel);
			
			buttons = new Vector.<Button>;
			
			for (var i:int=0;i<hero.attacks.length;i++){
				attackButton = new Button();
				attackButton.label = hero.attacks[i]["name"];//+"\nCost: " + hero.attacks[i]["cost"];
				attackButton.x = (SnowGame.width/4 * i + SnowGame.width/4) - 50;
				attackButton.y = SnowGame.height/2-50;
				addChild(attackButton);
				
				attackButton.onRelease.add(attack);
				buttons.push(attackButton);
			}

			addChild(textPanel = new TextPanel());
			textPanel.x +=25;
			textPanel.y +=5;

			
			var stat:StatPanel = new StatPanel();
			stat.y=600;
			addChild(stat);
		}
		
		public function setCallback(callback:Function):void
		{
			this.callback = callback;
		}
		
		private function closePanel(b:Button):void
		{
			textPanel.addMessage("Fluffy McSnugglebottom uses his cunning and runs away.\n\n", quit);
		}
		
		override public function switchedTo():void
		{
			battleMusic.play(true);
			var e:Enemy = enemyList[Math.floor(Math.random()*enemyList.length)];
			 
			enemy = new Enemy(e.name,e.health,e.dex);
			attackList = new Array();

			attackList.push(hero);
			attackList.push(enemy);
			attackList.sortOn("dex",Array.NUMERIC | Array.DESCENDING);
			attackTurn =0;

			disableButtons();
			textPanel.addMessage("You've encountered a " + enemy.name);
			if (enemy.dex>hero.dex){
				textPanel.addMessage("The " + enemy.name + " has the upper hand and prepare's its attack.",nextTurn);
			}else{
				textPanel.addMessage("The " + enemy.name + " is surprised.",nextTurn);
			}

			
		}
		
		private function enemyAttack(e:Enemy):void
		{
			var damage:int = enemy.attack().call(null,hero, enemy);
			
			if (damage == -1)
				textPanel.addMessage("The " + enemy.name + " goes in for an attack but misses!",nextTurn);
			else{
				textPanel.addMessage("The " + enemy.name + " attacks and deals \n" + damage + " points of damage.",nextTurn);
				SnowGame.flash(0.25,0xFF0000);
				SnowGame.quake(5,0.5);
				hurtSound.play();
			}
			
			hero.health -= damage;
			if (hero.health <1)
				textPanel.addMessage("The heroes of the world have defeated you...",youDied);
		}
		
		private function youDied():void{
			SnowGame.switchState(new EndState());			
		}
		
		private function nextTurn():void
		{
			healthField.text = "Health " + hero.health;

			if (attackTurn >= attackList.length)
				attackTurn =0;
			
			if (attackList[attackTurn] == hero){
				enableButtons();
				textPanel.addMessage("Fluffy's turn to attack.");
			}
				
			else{
				enemyAttack(attackList[attackTurn]);
			}
			attacker = attackList[attackTurn];
			attackTurn++;
		}
		

		private function attack(b:Button):void
		{
			hero.selectedAttack = hero.attacks[buttons.indexOf(b)];
			disableButtons();
			handleAttack();

		}
		
		private function handleAttack():void
		{
			//var 
			var target:BattleCharacter;
			
			if (attacker == enemy)
				target = hero;
			else
				target = enemy;
			
			
			
			var damage:int = attacker.attack().call(null,target,attacker);
			
			if (attacker == hero && hero.selectedAttack["type"] == Skills.HEAL){
				hero.health += damage;
				if (hero.health > hero.max_health) hero.health = hero.max_health;
				textPanel.addMessage(attacker.name + " licks himself and gains " + damage + " health.",nextTurn);
				healthSound.play();
				SnowGame.flash(0.25,0x00FF00);
			}
			else{
				
				if (damage == -1)
					textPanel.addMessage(attacker.name + " narrowly missed!",nextTurn);
				else{
					textPanel.addMessage(attacker.name + " hit " + target.name + " for " + damage + " damage.",nextTurn);
					target.health -= damage;
					attackSound.play();
					SnowGame.flash();
				}
				
				
			}
			
			if (enemy.health<1){
				textPanel.addMessage(target.name + " is totally destroyed!\n", quit);
				
				hero.currentXP += target.xp;
				if (hero.currentXP > hero.levelXP[0]){
					hero.level ++;
					hero.levelXP.shift();
					textPanel.addMessage(attacker.name + " gained a level!\nWelcome to Level "+ hero.level);
					textPanel.addMessage("Nice work!\n\n\n",quit);
				}
					
				
			}
			
		}
		
		private function disableButtons():void
		{
			for (var i:int=0;i<buttons.length;i++)
				buttons[i].isEnabled = false;
		}
		
		private function enableButtons():void
		{
			for (var i:int=0;i<buttons.length;i++)
				buttons[i].isEnabled = true;
		}
		private function quit():void{
			battleMusic.stop();
			battleWin.setGameState(SnowGame.previousState,callback);
			SnowGame.switchState(battleWin);
			
		}
	}
}