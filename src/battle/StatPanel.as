package battle
{
	import com.refrag.snow.SnowGame;
	import com.refrag.snow.SnowState;
	
	import feathers.controls.Button;
	import feathers.themes.MinimalMobileTheme;
	
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.text.TextField;
	
	public class StatPanel extends Sprite
	{
		
		private var ag:int=0;
		private var wt:int=0;
		private var sk:int=0;
		
		private var agText:TextField;
		private var wtText:TextField;
		private var skText:TextField;

		
		public function StatPanel()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE,init);
		}
		
		private function init(e:Event):void
		{
			//Background
			var q:Quad = new Quad(SnowGame.width,40,0xFFFFFF);
			q.alpha = 0.75;
			//q.y +=35;
			q.blendMode = BlendMode.SCREEN;			
			addChild(q);
			
			agText = new TextField(250,36,"AGGRESSION:","Verdana",22);
			agText.x = 20;
			agText.y = 5;
			addChild(agText);
			
			wtText = new TextField(250,36,"SEDUCTION:","Verdana",22);
			wtText.x = 20 + (SnowGame.width/3);
			wtText.y = 5;
			addChild(wtText);
			
			skText = new TextField(250,36,"INTELLECT:","Verdana",22);
			skText.x = 20 + (SnowGame.width/3 * 2);
			skText.y = 5;
			addChild(skText);
			
			addEventListener(Event.ENTER_FRAME,loop);
			
		}
		
		private function loop(e:Event):void
		{
			if (ag!=GameVars.aggression || wt!=GameVars.wit || sk!= GameVars.skill)
			{
				agText.text = "AGGRESSION: " + GameVars.aggression;
				ag = GameVars.aggression;
				
				wtText.text = "SEDUCTION: " + GameVars.wit;
				wt = GameVars.wit;
				
				skText.text = "INTELLECT: " + GameVars.skill;
				sk = GameVars.skill;
			}
			
		}
	}
}