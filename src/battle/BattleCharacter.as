package battle
{
	/*
	I'm a generic Battle Character Class.
	I have standard attributes and stats that are needed
	for all kinds of battle calculations.
	*/
	public class BattleCharacter
	{
		public var name:String;
		public var level:int;
		
		public var xp:int; // XP Worth

		public var selectedAttack:Object;
	
		public var max_health:int;
		public var health:int;
		
		public var dex:int;
		
		
		public var target:BattleCharacter;
		public var attacks:Array = new Array();
		
		
		public function BattleCharacter()
		{
		}

		
		/*
		I should totally be overridden unless I only have 1 skill.. which sounds boring.
		*/
		public function attack():Function
		{
			return selectedAttack["logic"];
			if (health>max_health) health = max_health;
		}
	}
}