package battle
{
	public class Enemy extends BattleCharacter
	{
		public function Enemy(name:String,heath:int=10,dex:int=11)	
		{
			
			this.name = name;
			level = 1;
			max_health = 10;
			health = 10;
			dex = 11;
			
			xp = 1;
			
			var skill:Object = {name:"Basic Attack", cost:5, logic:Skills.basicAttack};

			attacks.push(skill);
			
			selectedAttack = skill;
		}
	}
}