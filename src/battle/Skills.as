package battle
{
	/*
	I'm a pretty rad Class.
	I store Skill logic.
	Don't go getting me gunked up with other crap.
	*/
	public class Skills
	{
		
		public static const MELEE:String = "Melee";
		public static const HEAL:String = "Heal";
		
		public function Skills()
		{
		}
		
		public static function basicAttack(target:BattleCharacter, attacker:BattleCharacter):int
		{
			
			var damage:int;
			if (Roll.d20()<10)
				damage = -1;
			else
				damage = Roll.d6(2);
			
			
			return damage;
		}
		
		public static function superAttack(target:BattleCharacter, attacker:BattleCharacter):int
		{
			
			var damage:int;
			if (Roll.d20()<10)
				damage = -1;
			else
				damage = Roll.d6(5);
			
			
			return damage;
		}
		
		public static function selfHeal(self:BattleCharacter, notUsed:BattleCharacter):int
		{
			var heal:int;
			
			heal = Roll.d20();
			
			return heal;
		}

	}
}