package battle
{
	import com.refrag.snow.SnowGame;
	import com.refrag.snow.SnowSound;
	import com.refrag.snow.SnowState;
	
	import feathers.controls.Button;
	import feathers.themes.MinimalMobileTheme;
	
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.textures.Texture;
	
	
	public class BattleWin extends SnowState
	{
		
		[Embed(source = "../../data/battleStar1.png")] private static const Star3Bitmap:Class;

		[Embed(source="../../data/battleVictory.mp3") ] private static var battleMusic_data:Class;
		private var battleMusic:SnowSound;

		private var star3Tex:Texture;

		private var star3:Image;

		private var theme:MinimalMobileTheme;
		private var textPanel:TextPanel;
		
		
		private var aggressionButton:Button;
		private var witButton:Button;
		private var skillButton:Button;
		
		private var gameState:SnowState;
		private var callback:Function;
		
		private var select:Boolean = false;
		
		public function BattleWin()
		{
			battleMusic = new SnowSound();
			battleMusic.loadEmbedded(battleMusic_data);
			this.gameState = gameState;
			super();
			addEventListener(Event.ADDED_TO_STAGE,init);

		}
		
		private function init(e:Event):void
		{
			star3Tex = Texture.fromBitmap(new Star3Bitmap());
			star3 = new Image(star3Tex);
			star3.pivotX = star3.width >>1;
			star3.pivotY = star3.height >>1;
			
			star3.scaleX = 3.5;
			star3.scaleY = 3.5;
			star3.x = SnowGame.width /2;
			star3.y = SnowGame.height/2;
			star3.touchable = false;
			star3.alpha = 1;
			
			addChild(star3);
			theme = new MinimalMobileTheme(SnowGame.stage);
			
			var aggressionButton:Button = new Button();
			aggressionButton.label = "Aggression +1";
			aggressionButton.x = 100;
			aggressionButton.y = SnowGame.height/2-50;
			addChild(aggressionButton);
			aggressionButton.onRelease.add(selected);

			var witButton:Button = new Button();
			witButton.label = "Seduction +1";
			witButton.x = 100 + (SnowGame.width/3);
			witButton.y = SnowGame.height/2-50;
			addChild(witButton);
			witButton.onRelease.add(selected);
			
			var skillButton:Button = new Button();
			skillButton.label = "Intellect +1";
			skillButton.x = 100 + (SnowGame.width/3 * 2);
			skillButton.y = SnowGame.height/2-50;
			addChild(skillButton);
			skillButton.onRelease.add(selected);
			//Background
			var q:Quad = new Quad(SnowGame.width,96,0xFFFFFF);
			q.alpha = 0.75;
			//q.y +=35;
			q.blendMode = BlendMode.SCREEN;			
			addChild(q);
			
			var q2:Quad = new Quad(SnowGame.width,96,0xFFFFFF);
			q2.alpha = 0.5;
			q2.blendMode = BlendMode.SCREEN;
			q2.rotation = 0.04;
			q2.y -=35;
			addChild(q2);
			addChild(textPanel = new TextPanel());
			textPanel.x +=25;
			textPanel.y +=5;
			

			
			var stat:StatPanel = new StatPanel();
			stat.y=600;
			addChild(stat);
			addEventListener(Event.ENTER_FRAME,loop);

		}
		
		private function loop(e:EnterFrameEvent):void
		{
			star3.rotation -= -(e.passedTime);
		}
		
		public function setGameState(gameState:SnowState, callback:Function):void
		{
			this.gameState = gameState;
			this.callback = callback;
		}
		
		override public function switchedTo():void
		{
			select = false;
			battleMusic.play(true);
			textPanel.addMessage("Victory!\nChoose which skill you wish to upgrade...");
		}
		
		private function selected(b:Button):void
		{
			if (select) return
			select = true;
			
			
			if (b.label == "Aggression +1")
			{
				GameVars.aggression++;
				textPanel.addMessage("You feel your aggression level surge.\nWatch out world, here you come.\n\n\n\n",finishUp);
			}
			else if (b.label == "Seduction +1")
			{
				GameVars.wit++;
				textPanel.addMessage("Your Seduction grows.\nNo mortal can resist you.\n\n\n\n",finishUp);
			}
			else if (b.label == "Intellect +1")
			{
				GameVars.skill++;
				textPanel.addMessage("Your Intellect increases.\nYour evil geniousness has become even more genious-er.\n\n\n\n",finishUp);
			}
		}
		
		private function finishUp():void
		{
			battleMusic.stop();
			trace("Finishing...");
			SnowGame.switchState(gameState);
			callback.call();
		}
	}
}