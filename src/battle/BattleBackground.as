package battle
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.EnterFrameEvent;
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	import com.refrag.snow.SnowGame;
	
	public class BattleBackground extends Sprite
	{
		
		private var bitmap:Bitmap = new Bitmap(new BitmapData(1,1,true,0xFF3b263c));
		
		private var topArray:Array;
		private var bottomArray:Array;
		
		private var scales:Array;
		
		public function BattleBackground()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE,init);
			
		}
		
		private function init(e:Event):void
		{
			topArray = new Array();
			bottomArray = new Array();
			scales = new Array();
			
			for (var i:int =0 ;i<20;i++){
				var r:int = Math.random() * 96;
				scales.push(r);
				var img:Image = new Image(Texture.fromBitmap(bitmap));
				img.smoothing = TextureSmoothing.NONE;
				img.scaleX = 960;
				img.scaleY = 0;
				img.x=0;
				img.y=SnowGame.height/2;
				topArray.push(img);
				addChild(img);
				
				var img2:Image = new Image(Texture.fromBitmap(bitmap));
				img2.smoothing = TextureSmoothing.NONE;
				img2.scaleX = 960;
				img2.scaleY = 0;
				img2.x=0;
				img2.y=SnowGame.height/2;
				bottomArray.push(img2);
				addChild(img2);
			}
			
			addEventListener(Event.ENTER_FRAME,loop);
			
		}
		
		private function loop(e:EnterFrameEvent):void
		{
			for (var i:int = 0;i<topArray.length;i++)
			{
				topArray[i].y -= e.passedTime*100;
				topArray[i].scaleY += scales[i]*(e.passedTime*0.25);
				if (topArray[i].y >(SnowGame.height/2-100)) break;
				
				if (topArray[i].y < -200) {
					topArray[i].y = SnowGame.height/2 - 99;
					topArray[i].scaleY =1;
					topArray.push(topArray.pop());
				}
			}
			for (i = 0;i<bottomArray.length;i++)
			{
				bottomArray[i].y += e.passedTime*100;
				bottomArray[i].scaleY += scales[i]*(e.passedTime*0.25);

				
				if (bottomArray[i].y <(SnowGame.height/2+100)) break;
				if (bottomArray[i].y > SnowGame.height+200) {
					bottomArray[i].y = SnowGame.height/2+100;
					bottomArray[i].scaleY =1;

					bottomArray.push(bottomArray.pop());
				}
			}
		}
	}
}