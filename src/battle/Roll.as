package battle
{
	/*
	I like to roll dice.
	I have built in dice functions you can call.
	My dice are purple.
	*/
	public class Roll
	{
		public function Roll()
		{
		}
		
		public static function d6(count:int = 1):int
		{
			var roll:int;
			
			for (var i:int=0;i<count;i++)
				roll += d(6);
			
			return roll;
			
		}
		
		public static function d20(count:int = 1):int
		{
			var roll:int;
			
			for (var i:int=0;i<count;i++)
				roll += d(20);
			
			return roll;
		}
		
		private static function d(n:int):int
		{
			var roll:int = 0;
			while(roll==0)
			{
				roll = Math.ceil(Math.random()*(n));
			}
			return roll;
		}
	}
}