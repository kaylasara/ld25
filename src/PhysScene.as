package
{
	import com.refrag.snow.SnowBody;
	import com.refrag.snow.SnowCharacter;
	import com.refrag.snow.SnowGame;
	import com.refrag.snow.SnowPhysScene;
	import com.refrag.snow.SnowPhysSprite;
	import com.refrag.snow.SnowSound;
	
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	
	import ChatWindow;
	
	import battle.BattleState;
	import battle.StatPanel;
	
	import nape.callbacks.CbEvent;
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.shape.Polygon;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;

	public class PhysScene extends SnowPhysScene
	{
		[Embed(source = '../data/block1.csv', mimeType = 'application/octet-stream')] private var BlockCSV:Class;
		[Embed(source = '../data/block2.csv', mimeType = 'application/octet-stream')] private var BlockCSV2:Class;
		[Embed(source = '../data/block3.csv', mimeType = 'application/octet-stream')] private var BlockCSV3:Class;

		[Embed(source="../data/overworld.mp3") ] private static var overworldMusic_data:Class;
		[Embed(source="../data/enterBattle.mp3") ] private static var enterBattleSound_data:Class;


		
		public var paused:Boolean = false;
		
		//public var hero:Hero;
		private var hero:Hero;
		private var npc1:NPC;
		private var npc2:NPC2;
		private var npc3:NPC3;
		
		private var npcs:Array;
		
		private var touchManager:TouchManager;
		private var chatGame:ChatGame;
		private var battleState:BattleState;
		
		private var ground1:Ground;
		private var ground2:Ground;
		private var ground3:Ground;
		
		private var goalCbType:CbType = new CbType();
		private var timeout:Number = 0;
		
		private var scale:Number = 1;
		
		private var scale_time:Number = 0;

		private var starBattleEnter:StarBattleEnter;
		private var overworldMusic:SnowSound;
		private var enterBattleSound:SnowSound;


		
		public function PhysScene()
		{
			
			overworldMusic = new SnowSound();
			overworldMusic.loadEmbedded(overworldMusic_data);
			
			enterBattleSound = new SnowSound();
			enterBattleSound.loadEmbedded(enterBattleSound_data);
			
			npcs = new Array();
			npc1 = new NPC();
			npc2 = new NPC2();
			npc3 = new NPC3();
			
			npcs.push(npc1);
			npcs.push(npc2);
			npcs.push(npc3);
			
			touchManager = new TouchManager();
			ground1 = new Ground(new BlockCSV());
			ground2 = new Ground(new BlockCSV2());
			ground3 = new Ground(new BlockCSV3());
			addEventListener(Event.ADDED_TO_STAGE,start);
		}
		
		private function dropNPC():void
		{
			var n:SnowCharacter = npcs[GameVars.points-1];
			n.allowRotation = false;
			addEntity(n);
			n.x = 200 + (600* (GameVars.points-1));
			n.y = hero.y;
			var q:Quad = touchManager.setTouchObject(n);
			addChild(q);
			q.addEventListener(TouchEvent.TOUCH, npcInteraction);
		}
		
		private function start(e:Event):void
		{
			overworldMusic.play(true);
			addChild(touchManager);
			
			battleState = new BattleState();

			init();
			//addBox(new Rectangle(0,stage.stageHeight-20,stage.stageWidth,20));
			addBox(new Rectangle(0,0,20,stage.stageHeight));
			//addBox(new Rectangle(stage.stageWidth,0,20,stage.stageHeight));
			
			hero = new Hero();		
			hero.allowRotation = false;
			addEntity(hero);
			hero.x = 500;
			hero.y = 490;
			touchManager.init(hero, this);
			

			
			//stage.addEventListener(TouchEvent.TOUCH, onClick);
			addGoal();
			addGoal(1000);
			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, SnowBody.DYNAMIC, goalCbType, touchGoal));
			
			dropNPC();
			
			
			var floor:Body = new Body(BodyType.STATIC);
			floor.shapes.add(new Polygon(Polygon.rect(0,32,ground1.blockWidth-32,ground1.blockHeight-64)));
			floor.cbTypes.add(SnowBody.WORLD);
			newSimple(floor,ground1,new Vec2(0,500));
			
			var floor2:Body = new Body(BodyType.STATIC);
			floor2.shapes.add(new Polygon(Polygon.rect(0,32,ground2.blockWidth-32,ground2.blockHeight-64)));
			floor2.cbTypes.add(SnowBody.WORLD);
			newSimple(floor2,ground2,new Vec2(ground1.blockWidth,500+ground1.blockHeight-64-64*2));
			
			var floor3:Body = new Body(BodyType.STATIC);
			floor3.shapes.add(new Polygon(Polygon.rect(0,32,ground3.blockWidth-32,ground3.blockHeight-64)));
			floor3.cbTypes.add(SnowBody.BLOCK);
			newSimple(floor3,ground3,new Vec2(ground1.blockWidth + ground2.blockWidth - 64,500-64*2));
			var stat:StatPanel = new StatPanel();
			//stat.y=600;
			addChild(stat);
			addChild(starBattleEnter = new StarBattleEnter());
			

		}
		public function touchGoal(i:InteractionCallback):void
		{
			this.timeDialation = 0.0001;
			if (GameVars.blocked)
				ChatWindow.simpleMessage("Fluffy can not pass. His evil duties in this area are not yet complete.",restoreTime);
			else{
				i.int2.castBody.cbTypes.clear();
				i.int2.castBody.shapes.at(0).filter = SnowBody.NOCOLLIDE_FILTER;
				i.int2.castBody.graphic.visible=false;
				ChatWindow.simpleMessage("Confident in his world domination plans, Fluffy moves onward.",restoreTime);
				dropNPC();
				GameVars.blocked=true;
			}
			
		}
		private function goBattle():void
		{
			paused = true;
			SnowGame.switchState(battleState);
			
			battleState.setCallback(battleCallback);
			hero.velocityX=0;
		}
		private function battleCallback():void
		{
			overworldMusic.play(true);
			trace("Called back!");
			paused=false;	
		}
		
		override public function switchedTo():void{
			trace("Switched!");
			paused=false;
		}
		public function loop(passedTime:Number):void
		{
			if (paused) return;
			timeout -= passedTime;
			if (keys.pressed("A")) hero.moveLeft();
			if (keys.pressed("D")) hero.moveRight();
			if (keys.pressed("W")) hero.jump();
			if (keys.pressed("X")) {
				scale_time = 1;
				starBattleEnter.start(1,goBattle);
			}
			if (hero.velocityX != 0 && hero.onFloor && Math.random()*7<passedTime && timeout<0){
				overworldMusic.stop();
				enterBattleSound.play();
				timeout = 10;
				scale_time = 1;
				starBattleEnter.start(1,goBattle);

			}
			refocus(
				new Vec2(
					hero.body.position.x*scale,
					hero.body.position.y*scale-200)	
				,0.25);
			touchManager.updateTouchables();
			
			if (scale_time>0){
				scale = 1+(Math.abs(Math.cos(scale_time)));
				trace(scale);
				world.scaleX = scale;
				world.scaleY = scale;
				scale_time -= passedTime;
			}else {
				world.scaleX=1;
				world.scaleY=1;
				scale=1;
			}
				

		}
		private function npcInteraction(e:TouchEvent):void
		{
			trace("Touching!");
			if (paused) return;
			
			if (e.touches.length > 0){
				var touch:Touch = e.touches[0];
				if (touch.phase == TouchPhase.ENDED)
				{
					this.timeDialation = 0.0001;
					var h:SnowPhysSprite = touchManager.getTouchObject(e.touches[0].target);
					ChatWindow.message(h.conv, restoreTime,e.touches[0].target);
					paused=true;
					
				}
			}
		}
		
		private function restoreTime(character:Quad=null,result:Boolean=false):void
		{
			if (character && result){
				character.removeEventListener(TouchEvent.TOUCH, npcInteraction);
				GameVars.blocked = false;
				if (GameVars.points>3){
					ChatWindow.simpleMessage("Well fed, re-united with his toy and high on catnip Fluffy dozes off into a deep slumber plotting world domination.",endGame);
					return;
				}
			}
				
			paused =false;
			this.timeDialation = 1;
		}
		
		private function endGame(character:Quad=null,result:Boolean=false):void
		{
			ChatWindow.simpleMessage("Thank you for playing. - Kayla & Melinda",restart);
		}
		
		private function restart(character:Quad=null,result:Boolean=false):void
		{
			SnowGame.switchState(new EndState());
		}
		
		private function addGoal(x:int = 700):void
		{
			var vec:Vec2 = new Vec2(x,-100);
			var floor:Body = new Body(BodyType.STATIC);
			floor.shapes.add(new Polygon(Polygon.rect(0,0,16,1000)));
			floor.cbTypes.add(goalCbType);
			
			var bmd:BitmapData = new BitmapData(16,1000,true, 0x663b263c);
			var tex:Texture = Texture.fromBitmapData(bmd);
			var img:Image = new Image(tex);
			img.x=0;
			img.y=stage.stageHeight-20;
			newSimple(floor,img,vec);	
		}
		private function addBox(r:Rectangle):void
		{
			//Handle Inverted Boxes 
			if (r.width<0){
				r.width = Math.abs(r.width);
				r.x = r.x - r.width;
			}
			
			if (r.height<0){
				r.height = Math.abs(r.height);
				r.y = r.y - r.height;
			}
			
			if (r.width==0 || r.height==0)
				return;
			
			
			var vec:Vec2 = new Vec2(r.x,r.y);
			var floor:Body = new Body(BodyType.STATIC);
			floor.shapes.add(new Polygon(Polygon.rect(0,0,r.width,r.height)));
			floor.cbTypes.add(SnowBody.BLOCK);
			
			var bmd:BitmapData = new BitmapData(r.width,r.height,true, 0xFF9785a6);
			var tex:Texture = Texture.fromBitmapData(bmd);
			var img:Image = new Image(tex);
			img.x=0;
			img.y=stage.stageHeight-20;
			newSimple(floor,img,vec);
		}
		
		private function onClick(e:TouchEvent):void
		{
			
			//touchManager.handleIt(e, this);
			
		}
	}
}