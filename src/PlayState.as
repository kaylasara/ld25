package
{
	import com.refrag.snow.SnowGame;
	import com.refrag.snow.SnowState;
	
	import ChatWindow;
	
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	
	public class PlayState extends SnowState
	{
		private var main:PhysScene;
		
		public function PlayState()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, start);
			addEventListener(Event.RESIZE, handleResize);
			
		}
		
		private function start():void
		{
			
			//Physics container
			
			main = new PhysScene();
			addChild(main);
			
			ChatWindow.initialize();
			addChild(ChatWindow.sprite);
			addChild(ChatWindow.chatGame);
			
			addEventListener(Event.ENTER_FRAME, loop);
			
		}
		
		private function loop(event:EnterFrameEvent):void
		{
			//DataStore.passedTime = event.passedTime;
			
			if (!SnowGame.PAUSED)
			{
				SnowGame.TIME = SnowGame.TIME + event.passedTime;	
				main.update();
				main.loop(event.passedTime);
			}

			
		}
		private function handleResize() :void {
			trace("----!!! STAGE RESIZE !!!----");
			trace("Width: " + stage.stageWidth);
			trace("Height: " + stage.stageHeight);
		}
	}
}